package com.example.kbeatis.gymtrack;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Konstantin on 14.03.2017.
 */

public class ProgramDaysAdapter extends RecyclerView.Adapter<ProgramDaysAdapter.ViewHolder> {
    Context context;
    private String[] titles;
    private ProgramDaysAdapter.Listener listener;
    private int[] numberExercises;

    public static interface Listener{
        public void onLongClick(int position);
        public void onClick(int position);

    }

    public void setListener(ProgramDaysAdapter.Listener listener){
        this.listener=listener;
    }

    public ProgramDaysAdapter(Context context, String[] titles, int[] numberExercises){
        this.titles=titles;
        this.numberExercises = numberExercises;
        this.context = context;

    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout view) {
            super(view);
            cardView = view;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_program, parent, false);
        return new ProgramDaysAdapter.ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ProgramDaysAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardView = holder.cardView;
        TextView number = (TextView) cardView.findViewById(R.id.program_text_number_exercises);
        String string = context.getResources().getString(R.string.exercise_number);
        number.setText(string + " " + Integer.toString(numberExercises[position]));
        TextView textView = (TextView)cardView.findViewById(R.id.program_text);
        textView.setText(titles[position]);
        cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (listener!=null){
                    listener.onLongClick(position);
                }
                return true;
            }
        });
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
