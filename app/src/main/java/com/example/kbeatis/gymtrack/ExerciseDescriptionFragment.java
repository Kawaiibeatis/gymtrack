package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseDescriptionFragment extends Fragment {

    private int numberMuscle;
    private int numberExercise;
    private int tableLength=0;
    private int[] id;
    private int[] titles;
    private int[] idImageResource;
    private int[] description;
    Toolbar toolbar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        return inflater.inflate(R.layout.fragment_exercise_description, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        numberMuscle = getArguments().getInt("numberMuscle");
        numberExercise = getArguments().getInt("numberExercise");
        toolbar.setTitle(getString(R.string.nav_exercises));



        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("EXERCISE", null, null);
            id = new int[tableLength];
            titles = new int[tableLength];
            idImageResource = new int[tableLength];
            description = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();

            Cursor cursor = db.query("EXERCISE",
                    new String[]{ "TITLE", "ID_IMAGE_RESOURCE", "DESCRIPTION"},
                    "ID_MUSCLE = ?",
                    new String[] {Integer.toString(numberMuscle)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                titles[0] = cursor.getInt(0);
                idImageResource[0] = cursor.getInt(1);
                description[0] = cursor.getInt(2);
            }
            for (int i = 1;i < tableLength; i++) {
                if (cursor.moveToNext()){
                    titles[i] = cursor.getInt(0);
                    idImageResource[i] = cursor.getInt(1);
                    description[i] = cursor.getInt(2);
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        View view = getView();
        ImageView imageView = (ImageView) view.findViewById(R.id.exercise_description_image);
        TextView textView = (TextView) view.findViewById(R.id.exercise_description_text);
        imageView.setImageResource(idImageResource[numberExercise]);
        textView.setText(description[numberExercise]);
    }
}
