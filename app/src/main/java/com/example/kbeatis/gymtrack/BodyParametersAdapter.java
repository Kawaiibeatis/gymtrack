package com.example.kbeatis.gymtrack;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Konstantin on 03.04.2017.
 */

public class BodyParametersAdapter extends RecyclerView.Adapter<BodyParametersAdapter.ViewHolder> {


    private String[] titles;
    private BodyParametersAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }

    public void setListener (BodyParametersAdapter.Listener listener){
        this.listener = listener;
    }

    public BodyParametersAdapter(String[] titles){
        this.titles = titles;
    }
    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout view) {
            super(view);
            cardView = view;
        }
    }

    @Override
    public BodyParametersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardview = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_body_parameters, parent, false);
        return new BodyParametersAdapter.ViewHolder(cardview);
    }

    @Override
    public void onBindViewHolder(BodyParametersAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardview = holder.cardView;
        TextView textView = (TextView) cardview.findViewById(R.id.body_parameters_text);
        textView.setText(titles[position]);
        cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
