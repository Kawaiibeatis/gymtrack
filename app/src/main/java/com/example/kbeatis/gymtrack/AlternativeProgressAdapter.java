package com.example.kbeatis.gymtrack;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.res.Resources;

/**
 * Created by kbeatis on 10.03.17.
 */
public class AlternativeProgressAdapter extends RecyclerView.Adapter<AlternativeProgressAdapter.ViewHolder> {

    private int[] title;
    private boolean isEnabled;
    private Listener listener;
    private int selectedPos = 0;

    public  interface Listener{
        void onClick(int position);
        void onClickHelp(int position);
    }

    public void setEnable(boolean isEnabled){this.isEnabled = isEnabled;}

    public void setListener(Listener listener){
        this.listener=listener;
    }

    public AlternativeProgressAdapter(int[] title){
        this.title = title;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        public ViewHolder(CardView v){
            super(v);
            cardView = v;
        }
    }

    @Override
    public AlternativeProgressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_progress, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(final AlternativeProgressAdapter.ViewHolder holder, final int position) {
        final CardView cardView = holder.cardView;
        holder.cardView.setSelected(selectedPos == position);
        TextView mTextView = (TextView)cardView.findViewById(R.id.progress_title);
        ImageView mImageViewHelp = (ImageView) cardView.findViewById(R.id.progress_help);
        mImageViewHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener!=null) listener.onClickHelp(position);
            }
        });
        mTextView.setText(title[position]);
        cardView.setEnabled(isEnabled);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = holder.getLayoutPosition();
                notifyItemChanged(selectedPos);
                if (listener!=null) listener.onClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return title.length;
    }
}
