package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProgramDaysFragment extends Fragment {

    int tableLength;
    String[] titles;
    int id = 0;
    int[] id_day;
    RecyclerView recyclerView;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_program));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_program_days, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        id = getArguments().getInt("position");
        mainProgram();
        return recyclerView;
    }

    private void mainProgram(){

        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("DAY", "id_program = ?", id);
            titles = new String[tableLength];
            id_day = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor= db.query("DAY",
                    new String[]{ "title", "id_day"},
                    "id_program = ?",
                    new String[] {Integer.toString(id)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                titles[0] = cursor.getString(0);
                id_day[0] = cursor.getInt(1);
            }
            for (int i = 1;i < tableLength+1; i++) {
                if (cursor.moveToNext()) {
                    titles[i] = cursor.getString(0);
                    id_day[i] = cursor.getInt(1);
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        ProgramDaysAdapter adapter = new ProgramDaysAdapter(getActivity(), titles, getNumberExercises(id_day));
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter.setListener(new ProgramDaysAdapter.Listener() {
            @Override
            public void onLongClick (final int position){
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.delete)
                        .content(R.string.delete_question)
                        .positiveText(R.string.delete_positive)
                        .negativeText(R.string.delete_negative)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                                SQLiteDatabase db = gymTrackDatabaseHelper.getWritableDatabase();
                                gymTrackDatabaseHelper.deleteDay(db, id_day[position]);
                                db.close();

                                Toast toast = Toast.makeText(getActivity(),"Удаление", Toast.LENGTH_SHORT);
                                toast.show();

                                dialog.dismiss();
                                mainProgram();
                            }
                        })
                        .show();

            }

            @Override
            public void onClick(int position) {
                Fragment fragment = new ProgramEditFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id_day", id_day[position]);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }

        });

    }

    private int[] getNumberExercises(int[] id_day){
        int[] numberExercises = {};
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            numberExercises = new int[id_day.length];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            for (int i = 0; i < id_day.length; i++) {
                cursor = db.query("exday",
                        new String[]{"count (*) as numb"},
                        "id_day = ?",
                        new String[]{Integer.toString(id_day[i])},
                        null, null, null);
                if(cursor.moveToFirst()){
                    numberExercises[i] = cursor.getInt(0);
                }
                cursor.close();
            }
            db.close();
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return numberExercises;
    }

}
