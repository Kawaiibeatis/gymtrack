package com.example.kbeatis.gymtrack;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Konstantin on 12.03.2017.
 */

 class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ViewHolder> {


    private String[] titles;
    private ProgramAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
        public void onLongClick(int position);
    }


    public void setListener(ProgramAdapter.Listener listener){
        this.listener=listener;
    }

    public ProgramAdapter(String[] titles){
        this.titles = titles;
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout view) {
            super(view);
            cardView = view;
        }
    }
    @Override
    public ProgramAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_program, parent, false);
        return new ProgramAdapter.ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ProgramAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardView = holder.cardView;

        TextView textView = (TextView)cardView.findViewById(R.id.program_text);
        textView.setText(titles[position]);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onClick(position);
                }
            }
        });
        cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(listener!=null){
                    listener.onLongClick(position);
                }
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
