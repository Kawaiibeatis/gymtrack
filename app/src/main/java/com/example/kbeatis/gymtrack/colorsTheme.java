package com.example.kbeatis.gymtrack;

/**
 * Created by Konstantin on 27.03.2017.
 */

public class colorsTheme {

    int theme;
    int colorNumber;

    private int colorsTheme(int theme, int colorNumber) {
        this.theme = theme;
        this.colorNumber = colorNumber;

        int colorTheme = 0;

        switch (theme) {
            case 1:
                switch (colorNumber) {
                    case 1:
                        colorTheme = R.color.colorPrimary;
                        break;
                    case 2:
                        colorTheme = R.color.colorAccent;
                        break;
                    case 3:
                        colorTheme = R.color.colorAccentLight;
                        break;
                }
                break;
            case 2:
                switch (colorNumber) {
                    case 1:
                        colorTheme = R.color.colorPrimary2;
                        break;
                    case 2:
                        colorTheme = R.color.colorAccent2;
                        break;
                    case 3:
                        colorTheme = R.color.colorAccentLight2;
                        break;
                }
                break;
            default:
                break;
        }
        return colorTheme;
    }
}



