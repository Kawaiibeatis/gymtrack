package com.example.kbeatis.gymtrack;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class GraphExerciseActivity extends AppCompatActivity {

    public  String title;
    public int idImageResource;
    private ShareActionProvider shareActionProvider;
    public static final String EXTRA_NUMB = "number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_exercise);

        int number = (Integer)getIntent().getExtras().get(EXTRA_NUMB);

        try {
            SQLiteOpenHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            cursor = db.query("EXERCISE",
                    new String[]{ "TITLE", "ID_IMAGE_RESOURCE"},
                    "id_exercise = ?",
                    new String[] {Integer.toString(number)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                title = cursor.getString(0);
                idImageResource = cursor.getInt(1);
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        TextView textView = (TextView)findViewById(R.id.exercise_text);
        textView.setText(title);
        ImageView imageView = (ImageView)findViewById(R.id.exercise_image);
        imageView.setImageResource(idImageResource);
    }
}

