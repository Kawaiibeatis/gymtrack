package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgramFragment extends Fragment {

    private final String[] daysCount = {
            "  1  ", "  2  ", "  3  ", "  4  ",
            "  5  ", "  6  ", "  7  "
    };
    private Spinner spinner;
    private EditText nameInput;

    private int tableLength;
    private String[] titles;
    private int[] id_program;


    View view;
    RecyclerView recyclerView;
    FloatingActionButton fab;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_program));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_program, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.program_recycler);
        fab = (FloatingActionButton)view.findViewById(R.id.fab_program);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mainProgram();
        return view;
    }

    private void mainProgram(){

        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("program", null, null);
            titles = new String[tableLength];
            id_program = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("program",
                    new String[]{ "title", "id_program"},
                    null, null, null, null, null);
            if (cursor.moveToFirst()) {
                titles[0] = cursor.getString(0);
                id_program[0] = cursor.getInt(1);
            }
            for (int i = 1;cursor.moveToNext();i++){
                titles[i] = cursor.getString(0);
                id_program[i] = cursor.getInt(1);
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        ProgramAdapter adapter = new ProgramAdapter(titles);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter.setListener(new ProgramAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Fragment fragment = new ProgramDaysFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("position", id_program[position]);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }

            @Override
            public void onLongClick(final int position) {
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.delete)
                        .content(R.string.delete_question)
                        .positiveText(R.string.delete_positive)
                        .negativeText(R.string.delete_negative)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                                SQLiteDatabase db = gymTrackDatabaseHelper.getWritableDatabase();
                                gymTrackDatabaseHelper.deleteProgram(db, id_program[position]);
                                db.close();

                                Toast toast = Toast.makeText(getActivity(), R.string.delete, Toast.LENGTH_SHORT);
                                toast.show();

                                dialog.dismiss();
                                mainProgram();
                            }
                        })
                        .show();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                        .title(R.string.new_program_title)
                        .customView(R.layout.dialog_create_program, true)
                        .positiveText(R.string.new_program_positive)
                        .negativeText(R.string.new_program_negative)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                                SQLiteDatabase db = gymTrackDatabaseHelper.getWritableDatabase();
                                gymTrackDatabaseHelper.insertProgram(db, nameInput.getText().toString(), spinner.getSelectedItemPosition() + 1);

                                db.close();
                                dialog.dismiss();
                                mainProgram();
                            }
                        })
                        .build();

                spinner = (Spinner) dialog.getCustomView().findViewById(R.id.spinnerOfDaysCount);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, daysCount);
                spinner.setAdapter(adapter);

                final View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
                nameInput = (EditText) dialog.getCustomView().findViewById(R.id.nameOfProgram);
                nameInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        GymTrackDatabaseHelper db = GymTrackDatabaseHelper.getInstance(getActivity());

                        boolean accept;

                        accept = !(s.toString().trim().length() == 0 || s.toString().trim().length() > 20) && db.IsProgramCreatedAlready(s.toString());

                        db.close();

                        positiveAction.setEnabled(accept);
                    }
                });

                dialog.show();
                positiveAction.setEnabled(false);
            }
        });
    }
}
