package com.example.kbeatis.gymtrack;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Konstantin on 15.03.2017.
 */

public class ProgramEditAdapter extends RecyclerView.Adapter<ProgramEditAdapter.ViewHolder> {
    Context context;
    private int[] titles;
    private int[] numberExercises;
    private ProgramEditAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }


    public void setListener(ProgramEditAdapter.Listener listener){
        this.listener=listener;
    }

    public ProgramEditAdapter(Context context, int[] titles, int[] numberExercises){
        this.context = context;
        this.titles=titles;
        this.numberExercises = numberExercises;
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout view) {
            super(view);
            cardView = view;
        }
    }

    @Override
    public ProgramEditAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_program, parent, false);
        return new ProgramEditAdapter.ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ProgramEditAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardView = holder.cardView;
        TextView number = (TextView) cardView.findViewById(R.id.program_text_number_exercises);
        String string = context.getResources().getString(R.string.exercise_number);
        number.setText(string + " " + Integer.toString(numberExercises[position]));
        TextView textView = (TextView)cardView.findViewById(R.id.program_text);
        textView.setText(titles[position]);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
