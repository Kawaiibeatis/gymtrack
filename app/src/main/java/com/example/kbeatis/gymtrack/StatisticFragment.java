package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.db.chart.Tools;
import com.db.chart.model.LineSet;
import com.db.chart.view.ChartView;
import com.db.chart.view.LineChartView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticFragment extends Fragment {

    private int idExercise;

    private LineChartView mChart;
    private int tableLength=0;
    private ArrayList<Integer> weight= new ArrayList();
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_graph));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        idExercise = getArguments().getInt("ID_EXERCISE");
        View layout = inflater.inflate(R.layout.fragment_statistic, container, false);
        mChart = (LineChartView) layout.findViewById(R.id.stat_chart_line);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //test();
        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(getActivity());
        if (helper.getTableLength("RESULT", "id_exercise = ?", idExercise)==0){
            Toast toast= Toast.makeText(getContext(), "NO DATA",Toast.LENGTH_SHORT);
            toast.show();
        } else{
            produce(mChart);
        }

        return layout;
    }


    private void produce(LineChartView chart) {

        chart.dismiss();

        //рисовка графика
        LineSet graph = new LineSet();

        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("RESULT", "id_exercise = ?", idExercise);
//            weight = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            cursor = db.query("RESULT",
                    new String[]{ "WEIGHT"},
                    "ID_EXERCISE = ?",
                    new String[] {Integer.toString(idExercise)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                weight.add(cursor.getInt(0));
            }
            for (int i = 1;i < tableLength; i++) {
                if (cursor.moveToNext()){
                    weight.add(cursor.getInt(0));
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }


        while (weight.size()>20){
            weight.remove(0);
        }
        if (weight.size() > 0){
            int maxW = -1, minW = 1000;
            for (int i =0;i<weight.size();i++) {
                graph.addPoint(Integer.toString(i+1),  weight.get(i));
                if (maxW < weight.get(i)) {
                    maxW = weight.get(i);
                }
                if (minW > weight.get(i)) {
                    minW = weight.get(i);
                }
            }
            //graph.addPoint("asd", 30);

            //graph.setSmooth(true);

            graph.setColor(getResources().getColor(R.color.transparentColor))
                    .setDotsStrokeThickness(Tools.fromDpToPx(3))
                    //       .setDotsStrokeColor(Color.parseColor("#FF365EAF"))
                    //       .setDotsColor(Color.parseColor("#FF365EAF"));
                    .setDotsStrokeColor(getResources().getColor(R.color.graph_color2))
                    .setDotsColor(getResources().getColor(R.color.transparentColor));
            chart.addData(graph);

            //параметры сетки графика
            Paint gridPaint = new Paint();
            gridPaint.setColor(getResources().getColor(R.color.transparentColor));
            gridPaint.setStyle(Paint.Style.FILL);
            gridPaint.setAntiAlias(false);
            gridPaint.setStrokeWidth(Tools.fromDpToPx(1.2f));

            //параметры осей и отрисовка
            chart.setBorderSpacing(Tools.fromDpToPx(7))
                    .setAxisBorderValues(minW / 10 * 10 - 5, maxW / 10 * 10 + 5, 5)
                    //        .setAxisColor(Color.parseColor("#FF365EAF"))
                    //        .setLabelsColor(Color.parseColor("#FF365EAF"))
                    .setAxisColor(getResources().getColor(R.color.transparentColor))
                    .setLabelsColor(getResources().getColor(R.color.transparentColor))
                    .setGrid((maxW / 10 * 10  - minW / 10 * 10 + 10)/5, 0, gridPaint);

            chart.show();
        }
    }
/*
    private void test(){
        mChart.dismiss();
        String[] val = {"val1", "val2", "val3"};
        float[] fl = {1f,2f,3f};
        LineSet dataset = new LineSet(val, fl);

        dataset.addPoint("val4", 4);
        //mChart.show();
        dataset.setColor(Color.RED)
                .setDotsStrokeThickness(Tools.fromDpToPx(3))
                //       .setDotsStrokeColor(Color.parseColor("#FF365EAF"))
                //       .setDotsColor(Color.parseColor("#FF365EAF"));
                .setDotsStrokeColor(getResources().getColor(R.color.colorPrimary))
                .setDotsColor(getResources().getColor(R.color.colorPrimary));
        mChart.addData(dataset);

        Paint gridPaint = new Paint();
        gridPaint.setColor(getResources().getColor(R.color.colorPrimary));
        gridPaint.setStyle(Paint.Style.FILL);
        gridPaint.setAntiAlias(false);
        gridPaint.setStrokeWidth(Tools.fromDpToPx(1.2f));

        //параметры осей и отрисовка
        mChart.setBorderSpacing(Tools.fromDpToPx(7))
                .setAxisBorderValues(0,100, 5)
                //        .setAxisColor(Color.parseColor("#FF365EAF"))
                //        .setLabelsColor(Color.parseColor("#FF365EAF"))
                .setAxisColor(getResources().getColor(R.color.colorPrimary))
                .setLabelsColor(getResources().getColor(R.color.colorPrimary))
                .setStep(5)
                .setGrid(100,100, gridPaint);

        mChart.show();
    }*/
}
