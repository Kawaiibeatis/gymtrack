package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseExerciseFragment extends Fragment {


    private int tableLength=0;
    private int[] titles;
    private int[] idImageResource;
    private int[] idMuscle;
    private int numberMuscle = 2;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_exercises));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView exerciseRecycler = (RecyclerView) inflater.inflate(R.layout.fragment_graph, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        numberMuscle = getArguments().getInt("position")+1;
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("EXERCISE", "ID_MUSCLE = ?", numberMuscle);
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            titles = new int[tableLength];
            idImageResource = new int[tableLength];
            idMuscle = new int[tableLength];

            Cursor cursor = db.query("EXERCISE",
                    new String[]{ "TITLE", "ID_IMAGE_RESOURCE", "ID_MUSCLE"},
                    "ID_MUSCLE = ?",
                    new String[] {Integer.toString(numberMuscle)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                titles[0] = cursor.getInt(0);
                idImageResource[0] = cursor.getInt(1);
                idMuscle[0] = cursor.getInt(2);
            }
            for (int i = 1;i < tableLength; i++) {
                if (cursor.moveToNext()){
                    titles[i] = cursor.getInt(0);
                    idImageResource[i] = cursor.getInt(1);
                    idMuscle[i] = cursor.getInt(2);
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        ExerciseExerciseAdapter adapter = new ExerciseExerciseAdapter(titles, idImageResource);
        exerciseRecycler.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        exerciseRecycler.setLayoutManager(layoutManager);


        adapter.setListener(new ExerciseExerciseAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Fragment fragment = new ExerciseDescriptionFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("numberMuscle", numberMuscle);
                bundle.putInt("numberExercise", position);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }
        });

        return exerciseRecycler;
    }

}
