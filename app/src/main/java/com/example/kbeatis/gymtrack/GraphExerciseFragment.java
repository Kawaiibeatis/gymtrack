package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public class GraphExerciseFragment extends Fragment {

    private int tableLength=0;
    private int[] id;
    private int[] titles;
    private int[] idImageResource;
    private int[] idMuscle;
    private int numberMuscle = 2;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_graph));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView graphRecycler = (RecyclerView) inflater.inflate(R.layout.fragment_graph, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        numberMuscle = getArguments().getInt("position")+1;

        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("EXERCISE", "ID_MUSCLE = ?", numberMuscle);
            id = new int[tableLength];
            titles = new int[tableLength];
            idImageResource = new int[tableLength];
            idMuscle = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("EXERCISE",
                    new String[]{"ID_EXERCISE", "TITLE", "ID_IMAGE_RESOURCE", "ID_MUSCLE"},
                    "ID_MUSCLE = ?",
                    new String[] {Integer.toString(numberMuscle)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                id[0]=cursor.getInt(0);
                titles[0] = cursor.getInt(1);
                idImageResource[0] = cursor.getInt(2);
                idMuscle[0] = cursor.getInt(3);
            }
            for (int i = 1;i < tableLength; i++) {
                if (cursor.moveToNext()){
                    id[i]=cursor.getInt(0);
                    titles[i] = cursor.getInt(1);
                    idImageResource[i] = cursor.getInt(2);
                    idMuscle[i] = cursor.getInt(3);
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        GraphExerciseAdapter adapter = new GraphExerciseAdapter(titles, idImageResource);
        graphRecycler.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        graphRecycler.setLayoutManager(layoutManager);

        adapter.setListener(new GraphExerciseAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Fragment fragment = new StatisticFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("ID_EXERCISE", id[position]);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }
        });
        return graphRecycler;
    }

}
