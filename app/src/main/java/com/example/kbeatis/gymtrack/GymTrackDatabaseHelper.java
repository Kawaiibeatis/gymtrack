package com.example.kbeatis.gymtrack;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.speech.tts.Voice;
import android.support.annotation.Nullable;

import java.util.Date;

/**
 * Created by kbeatis on 28.02.17.
 */

public class GymTrackDatabaseHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "gymtrack";
    private static final int DB_VERSION = 1;
    private static final String TITLE = "title";
    private static final String DATE = "date";
    private static final String TABLE_MUSCLE = "muscle";
    private static final String DESCRIPTION = "description";
    private static final String TABLE_EXERCISE = "exercise";
    private static final String ID_MUSCLE = "id_muscle";
    private static final String ID_EXERCISE = "id_exercise";
    private static final String TABLE_DAY = "day";
    private static final String ID_DAY = "id_day";
    private static final String TABLE_EXDAY = "exday";
    private static final String TABLE_PROGRAM = "program";
    private static final String ID_PROGRAM = "id_program";
    private static final String TABLE_DAYPROG = "dayprog";
    private static final String ID_IMAGE_RESOURCE = "id_image_resource";
    private static final String AMOUNT = "amount";
    private static final String TABLE_PROGRESS = "progress";
    private static final String ID_PROGRESS = "id_progress";
    private static final String ID_RESULT = "id_result";
    private static final String TABLE_RESULT = "result";
    private static final String APPROACH = "approach";
    private static final String WEIGHT = "weight";
    private static final String REPEATS = "repeats";
    private static final String TABLE_PROGRESSRESULT = "progressresult";

    private static GymTrackDatabaseHelper mInstance = null;
    private Context context;

    public static synchronized GymTrackDatabaseHelper getInstance(Context ctx) {

        if (mInstance == null) {
            mInstance = new GymTrackDatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    GymTrackDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        //таблица упражнений
//        String CREATE_EXERCISES_TABLE = "CREATE TABLE " + TABLE_EXERCISES + "("
//                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + TRAINING_NUMBER + " INT,"
//                + MAX_WEIGHT + " TEXT,"
//                + AVG_WEIGHT + " TEXT" + ")";
//        db.execSQL(CREATE_EXERCISES_TABLE);
//        //таблица тренировок
//        String CREATE_TRAININGS_TABLE = "CREATE TABLE " + TABLE_TRAININGS + "("
//                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + DATE + " TEXT,"
//                + PROGRAM + " TEXT,"
//                + DAY + " TEXT,"
//                + MARK + " INT" + ")";
//        db.execSQL(CREATE_TRAININGS_TABLE);
//        //таблица программ
//        String CREATE_PROGRAMS_TABLE = "CREATE TABLE " + TABLE_PROGRAMS + "("
//                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + KEY_NAME + " TEXT,"
//                + DAYS + " INT" + ")";
//        db.execSQL(CREATE_PROGRAMS_TABLE);
//        //таблица упражнений, которые используются в программах
//        String CREATE_USINGEX_TABLE = "CREATE TABLE " + TABLE_USING_EX + "("
//                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + PROGRAM + " TEXT,"
//                + DAY + " INT" + ")";
//        db.execSQL(CREATE_USINGEX_TABLE);
//        //таблица для библиотеки
//        String CREATE_EXWITHRESOURCES_TABLE = "CREATE TABLE " + TABLE_EXWITHRESOURCES + "("
//                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
//                + TITLE + " TEXT,"
//                + DESCRIPTION + " TEXT,"
//                + MUSCLEID + " INT" + ")";
//        db.execSQL(CREATE_EXWITHRESOURCES_TABLE);

        //База Данных с Базой Данных
        String CREATE_MUSCLE_TABLE = "CREATE TABLE " + TABLE_MUSCLE + "("
                + ID_MUSCLE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TITLE + " INTEGER,"
                + ID_IMAGE_RESOURCE + " INTEGER"
                + ");";
        db.execSQL(CREATE_MUSCLE_TABLE);
        insertMuscle(db, R.string.muscle_abs, R.drawable.muscle_abs_category);//1
        insertMuscle(db, R.string.muscle_back, R.drawable.muscle_back_category);//2
        insertMuscle(db, R.string.muscle_biceps, R.drawable.muscle_biceps_category);//3
        insertMuscle(db, R.string.muscle_calves, R.drawable.muscle_calves_category);//4
        insertMuscle(db, R.string.muscle_chest, R.drawable.muscle_chest_category);//5
        insertMuscle(db, R.string.muscle_deltas, R.drawable.muscle_deltas_category);//6
        insertMuscle(db, R.string.muscle_forearm, R.drawable.muscle_forearm_category);//7
        insertMuscle(db, R.string.muscle_hips, R.drawable.muscle_hips_category);//8
        insertMuscle(db, R.string.muscle_neck, R.drawable.muscle_neck_category);//9
        insertMuscle(db, R.string.muscle_triceps, R.drawable.muscle_triceps_category);//10


        String CREATE_EXERCISE_TABLE = "CREATE TABLE " + TABLE_EXERCISE + "("
                + ID_EXERCISE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TITLE + " INTEGER,"
                + DESCRIPTION + " INTEGER,"
                + ID_MUSCLE + " INTEGER,"
                + ID_IMAGE_RESOURCE + " INTEGER,"
                + " FOREIGN KEY ("+ ID_MUSCLE +") REFERENCES " + TABLE_MUSCLE + "("+ ID_MUSCLE +")"
                + ");";
        db.execSQL(CREATE_EXERCISE_TABLE);
        insertExercise(db, R.string.ex_1, R.string.descr_ex_1, 3, R.drawable.desc_1_1_1);
        insertExercise(db, R.string.ex_2, R.string.descr_ex_2, 3, R.drawable.desc_1_2_1);
        insertExercise(db, R.string.ex_3, R.string.descr_ex_3, 3, R.drawable.desc_1_3_1);
        insertExercise(db, R.string.ex_4, R.string.descr_ex_4,3, R.drawable.desc_1_4_1);
        insertExercise(db, R.string.ex_5, R.string.descr_ex_5, 3, R.drawable.desc_1_5_1);
        insertExercise(db, R.string.ex_6, R.string.descr_ex_6, 10, R.drawable.desc_2_1_1);
        insertExercise(db, R.string.ex_7, R.string.descr_ex_7,10,R.drawable.desc_2_2_1);
        insertExercise(db, R.string.ex_8, R.string.descr_ex_8,10,R.drawable.desc_2_3_1);
        insertExercise(db, R.string.ex_9, R.string.descr_ex_9,10,R.drawable.desc_2_4_1);
        insertExercise(db, R.string.ex_10, R.string.descr_ex_10,10,R.drawable.desc_2_5_1);
        insertExercise(db, R.string.ex_11, R.string.descr_ex_11,6,R.drawable.desc_3_1_1);
        insertExercise(db, R.string.ex_12, R.string.descr_ex_12,6,R.drawable.desc_3_2_1);
        insertExercise(db, R.string.ex_13, R.string.descr_ex_13,6,R.drawable.desc_3_3_1);
        insertExercise(db, R.string.ex_14, R.string.descr_ex_14,6,R.drawable.desc_3_4_1);
        insertExercise(db, R.string.ex_15, R.string.descr_ex_15,6,R.drawable.desc_3_5_1);
        insertExercise(db, R.string.ex_16, R.string.descr_ex_16,2,R.drawable.desc_4_1_1);
        insertExercise(db, R.string.ex_17, R.string.descr_ex_17,2,R.drawable.desc_4_2_1);
        insertExercise(db, R.string.ex_18, R.string.descr_ex_18,2,R.drawable.desc_4_3_1);
        insertExercise(db, R.string.ex_19, R.string.descr_ex_19,2,R.drawable.desc_4_4_1);
        insertExercise(db, R.string.ex_20,  R.string.descr_ex_20,  2, R.drawable.desc_4_5_1);
        insertExercise(db, R.string.ex_21,  R.string.descr_ex_21,  5, R.drawable.desc_5_1_1);
        insertExercise(db, R.string.ex_22,  R.string.descr_ex_22,  5, R.drawable.desc_5_2_1);
        insertExercise(db, R.string.ex_23,  R.string.descr_ex_23,  10, R.drawable.desc_5_3_1);
        insertExercise(db, R.string.ex_24,  R.string.descr_ex_24,  5, R.drawable.desc_5_4_1);
        insertExercise(db, R.string.ex_25,  R.string.descr_ex_25,  10, R.drawable.desc_5_5_1);
        insertExercise(db, R.string.ex_26,  R.string.descr_ex_26,  8, R.drawable.desc_6_1_1);
        insertExercise(db, R.string.ex_27,  R.string.descr_ex_27,  8, R.drawable.desc_6_2_1);
        insertExercise(db, R.string.ex_28,  R.string.descr_ex_28,  4, R.drawable.desc_6_3_1);
        insertExercise(db, R.string.ex_29,  R.string.descr_ex_29,  8, R.drawable.desc_6_4_1);
        insertExercise(db, R.string.ex_30,  R.string.descr_ex_30,  8, R.drawable.desc_6_5_1);
        insertExercise(db, R.string.ex_31,  R.string.descr_ex_31,  7, R.drawable.desc_7_1_1);
        insertExercise(db, R.string.ex_32,  R.string.descr_ex_32,  7, R.drawable.desc_7_2_1);
        insertExercise(db, R.string.ex_33,  R.string.descr_ex_33,  7, R.drawable.desc_7_3_1);
        insertExercise(db, R.string.ex_34,  R.string.descr_ex_34,  7, R.drawable.desc_7_4_1);
        insertExercise(db, R.string.ex_35,  R.string.descr_ex_35,  1, R.drawable.desc_8_1_1);
        insertExercise(db, R.string.ex_36,  R.string.descr_ex_36,  1, R.drawable.desc_8_2_1);
        insertExercise(db, R.string.ex_37,  R.string.descr_ex_37,  1, R.drawable.desc_8_3_1);
        insertExercise(db, R.string.ex_38,  R.string.descr_ex_38,  1, R.drawable.desc_8_4_1);
        insertExercise(db, R.string.ex_39,  R.string.descr_ex_39,  1, R.drawable.desc_8_5_1);

        String CREATE_DAY_TABLE = "CREATE TABLE " + TABLE_DAY + "("
                + ID_DAY + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TITLE + " TEXT,"
                + ID_PROGRAM + " INTEGER"
                + ");";
        db.execSQL(CREATE_DAY_TABLE);
        //insertDay(db, "Day 1", 1);
        //insertDay(db, "Day 2", 1);

        String CREATE_TABLE_EXDAY = "CREATE TABLE " + TABLE_EXDAY + "("
                + ID_EXERCISE + " INTEGER,"
                + ID_DAY + " INTEGER,"
                + " FOREIGN KEY ("+ID_EXERCISE+") REFERENCES " + TABLE_EXERCISE + "("+ ID_EXERCISE +"),"
                + " FOREIGN KEY ("+ID_DAY+") REFERENCES " + TABLE_DAY + "("+ ID_DAY +")"
                + ");";
        db.execSQL(CREATE_TABLE_EXDAY);
        insertExInDay(db,1, 1);
        insertExInDay(db, 2, 1);
        insertExInDay(db, 3, 1);
        insertExInDay(db, 21, 1);
        insertExInDay(db, 22, 1 );
        insertExInDay(db, 24, 1 );
        insertExInDay(db, 6,2);
        insertExInDay(db, 7,2);
        insertExInDay(db, 8,2);
        insertExInDay(db, 16,2);
        insertExInDay(db, 17,2);


        String CREATE_PROGRAM_TABLE = "CREATE TABLE " + TABLE_PROGRAM + "("
                + ID_PROGRAM + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TITLE + " TEXT,"
                + AMOUNT + " INTEGER"
                + ");";
        db.execSQL(CREATE_PROGRAM_TABLE);
        insertProgram(db, "Program by Arni", 2);
        insertProgram(db, "Jack Black's Program", 3);




        /*String CREATE_TABLE_DAYPROG = "CREATE TABLE " + TABLE_DAYPROG + "("
                + ID_DAY + " INTEGER,"
                + ID_PROGRAM + " INTEGER,"
                + " FOREIGN KEY ("+ID_DAY+") REFERENCES " + TABLE_DAY + "("+ ID_DAY +"),"
                + " FOREIGN KEY ("+ID_PROGRAM+") REFERENCES " + TABLE_PROGRAM + "("+ ID_PROGRAM +")"
                + ");";
        db.execSQL(CREATE_TABLE_DAYPROG);
        insertDayInProgramm(db, 1, 1);
        insertDayInProgramm(db, 2, 1);*/

        String CREATE_TABLE_RESULT = "CREATE TABLE " + TABLE_RESULT + "("
                +ID_RESULT + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                +ID_EXERCISE + " INTEGER,"
                + APPROACH + " INTEGER,"
                + REPEATS + " INTEGER,"
                + WEIGHT + " INTEGER,"
                + ID_PROGRESS + " INTEGER,"
                + " FOREIGN KEY ("+ID_EXERCISE+") REFERENCES " + TABLE_EXERCISE + "("+ ID_EXERCISE +"),"
                + " FOREIGN KEY ("+ID_PROGRESS+") REFERENCES " + TABLE_PROGRESS + "("+ ID_PROGRESS +")"
                + ");";
        db.execSQL(CREATE_TABLE_RESULT);
        insertResult(db, 7, 1, 12, 15, 3);
        insertResult(db, 7, 2, 12, 20, 3);
        insertResult(db, 7, 3, 8, 30, 3);
        insertResult(db, 16, 1, 12, 50, 3);
        insertResult(db, 16, 2, 12, 55, 3);
        insertResult(db, 16, 3, 12, 70, 3);
        insertResult(db, 1, 1, 12, 50, 3);
        insertResult(db, 1, 2, 12, 55, 3);
        insertResult(db, 1, 2, 12, 65, 3);
        insertResult(db, 2, 1, 12, 50, 3);
        insertResult(db, 2, 2, 12, 40, 3);
        insertResult(db, 2, 2, 12, 80, 3);
        insertResult(db, 3, 1, 12, 100, 3);
        insertResult(db, 3, 2, 12, 40, 3);
        insertResult(db, 3, 2, 12, 200, 3);
        insertResult(db, 4, 1, 12, 10, 3);
        insertResult(db, 4, 2, 12, 30, 3);
        insertResult(db, 4, 2, 12, 5, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);
        insertResult(db, 5, 1, 12, 10, 3);
        insertResult(db, 5, 2, 12, 60, 3);
        insertResult(db, 5, 2, 12, 10, 3);
        insertResult(db, 5, 4, 12, 100, 3);
        insertResult(db, 5, 5, 12, 50, 3);
        insertResult(db, 5, 6, 12, 150, 3);

        String CREATE_TABLE_PROGRESS = "CREATE TABLE " + TABLE_PROGRESS + "("
                + ID_PROGRESS + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ID_PROGRAM + " INTEGER,"
                + ID_DAY + " INTEGER,"
                + DATE + " DATE,"
                + " FOREIGN KEY ("+ID_DAY+") REFERENCES " + TABLE_DAY + "("+ ID_DAY +"),"
                + " FOREIGN KEY ("+ID_PROGRAM+") REFERENCES " + TABLE_PROGRAM + "("+ ID_PROGRAM +")"
                + ");";
        db.execSQL(CREATE_TABLE_PROGRESS);
        insertProgress(db, 1, 1, "2016.02.25");
        insertProgress(db, 1, 2, "2016.02.27");
        insertProgress(db, 1, 1, "2016.02.30");
        insertProgress(db, 2, 1, "2017.03.01");


        String CREATE_TABLE_PROGRESSRESULT = "CREATE TABLE " + TABLE_PROGRESSRESULT + "("
                + ID_PROGRESS + " INTEGER,"
                + ID_RESULT + " INTEGER,"
                + " FOREIGN KEY ("+ID_PROGRESS+") REFERENCES " + TABLE_PROGRESS + "("+ ID_PROGRESS +"),"
                + " FOREIGN KEY ("+ID_RESULT+") REFERENCES " + TABLE_RESULT + "("+ ID_RESULT +")"
                + ");";
        db.execSQL(CREATE_TABLE_PROGRESSRESULT);

        String CREATE_TABLE_HEIGHT = "CREATE TABLE HEIGHT("
                + "_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "HEIGHT FLOAT,"
                + "DATE DATE);";
        db.execSQL(CREATE_TABLE_HEIGHT);

        insertHeight(db, 178, "2017.03.03");

        String CREATE_TABLE_WEIGHT = "CREATE TABLE WEIGHT("
                + "_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "WEIGHT FLOAT,"
                + "DATE DATE);";
        db.execSQL(CREATE_TABLE_WEIGHT);
        insertWeight(db, 65, "2017.04.04");

        String CREATE_TABLE_CURRENT_THEME = "CREATE TABLE CURRENTTHEME("
                + "_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "THEME INTEGER);";
        db.execSQL(CREATE_TABLE_CURRENT_THEME);
        insertCurrentTheme(db, 0);
        insertCurrentTheme(db, 1);
        insertCurrentTheme(db, 2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        if (oldVersion < 1){}
    }


    //методы занесения данных в таблицу
    private static void insertMuscle(SQLiteDatabase db, int title, int resourceID){
        ContentValues muscleValues = new ContentValues();
        muscleValues.put(TITLE, title);
        muscleValues.put(ID_IMAGE_RESOURCE, resourceID);
        db.insert(TABLE_MUSCLE, null, muscleValues);
    }

    private static void insertExercise(SQLiteDatabase db, int title, int description, int id_muscle, int resourceID){
        ContentValues exerciseValues = new ContentValues();
        exerciseValues.put(TITLE, title);
        exerciseValues.put(DESCRIPTION, description);
        exerciseValues.put(ID_MUSCLE, id_muscle);
        exerciseValues.put(ID_IMAGE_RESOURCE, resourceID);
        db.insert(TABLE_EXERCISE, null, exerciseValues);
    }

    private static void insertDay(SQLiteDatabase db, String title, int idProgram){
        ContentValues dayValues = new ContentValues();
        dayValues.put(TITLE, title);
        dayValues.put(ID_PROGRAM, idProgram);
        db.insert(TABLE_DAY, null, dayValues);
    }

    public void deleteDay(SQLiteDatabase db, int id_day){
        db.delete(TABLE_DAY, ID_DAY + " = ?", new String[] {Integer.toString(id_day)});
    }

    public void insertExInDay(SQLiteDatabase db, int id_exercise, int id_day){
        ContentValues exInDayValues = new ContentValues();
        exInDayValues.put(ID_EXERCISE, id_exercise);
        exInDayValues.put(ID_DAY, id_day);
        db.insert(TABLE_EXDAY, null, exInDayValues);
    }

    public void deleteExInDay(SQLiteDatabase db, int id_exercise){
        db.delete(TABLE_EXDAY, ID_EXERCISE + " = ?", new String[] {Integer.toString(id_exercise)});
    }
    public void insertProgram(SQLiteDatabase db,String title, int amount){
        ContentValues programValues = new ContentValues();
        programValues.put(TITLE, String.valueOf(title));
        programValues.put(AMOUNT, amount);
        db.insert(TABLE_PROGRAM, null, programValues);
        String[]days= context.getResources().getStringArray(R.array.days_array);
        for (int i = 0; i < amount; i++){
            insertDay(db, days[i], searchProgram(db, title));
        }
    }


    private int searchProgram(SQLiteDatabase db, String title){
        Cursor cursor = db.query(TABLE_PROGRAM, new String[]{"ID_PROGRAM"}, "TITLE = ?" ,  new String[] {title},
                null, null, null);
        int id = 0;
        if (cursor.moveToFirst()){
            id = cursor.getInt(0);
        }
        cursor.close();
        return id;
    }

    public void deleteProgram(SQLiteDatabase db, int id_program){
        Cursor cursor = db.query(TABLE_DAY, new String[]{"ID_DAY"}, ID_PROGRAM + " = ?", new String[]{Integer.toString(id_program)},
                null,null,null);
        if (cursor.moveToFirst()){
            deleteDay(db, cursor.getInt(0));
        }
        while (cursor.moveToNext()){
            deleteDay(db, cursor.getInt(0));
        }
        cursor.close();
        db.delete(TABLE_PROGRAM, ID_PROGRAM + " = ?", new String[] {Integer.toString(id_program)});
    }

    /*private static void  insertDayInProgramm(SQLiteDatabase db, int id_day, int id_program){
        ContentValues dayInProgrammValues = new ContentValues();
        dayInProgrammValues.put(ID_DAY, id_day);
        dayInProgrammValues.put(ID_PROGRAM, id_program);
        db.insert(TABLE_DAYPROG, null, dayInProgrammValues);
    }*/

    public void  insertResult (SQLiteDatabase db, int id_exercise, int approach, int repeats, int weight, int id_progress){
        ContentValues resultValues = new ContentValues();
        resultValues.put(ID_EXERCISE, id_exercise);
        resultValues.put(APPROACH, approach);
        resultValues.put(REPEATS, repeats);
        resultValues.put(WEIGHT, weight);
        resultValues.put(ID_PROGRESS, id_progress);
        db.insert(TABLE_RESULT, null, resultValues);
    }

    public void  insertProgress (SQLiteDatabase db, int id_program, int id_day, String date){
        ContentValues progressValues = new ContentValues();
        progressValues.put(ID_PROGRAM, id_program);
        progressValues.put(ID_DAY, id_day);
        progressValues.put(DATE, date);
        db.insert(TABLE_PROGRESS, null, progressValues);
    }

    public void insertProgressResult(SQLiteDatabase db, int id_progress, int id_result){
        ContentValues progressResultValues = new ContentValues();
        progressResultValues.put(ID_PROGRESS, id_progress);
        progressResultValues.put(ID_RESULT, id_result);
        db.insert(TABLE_PROGRESSRESULT, null, progressResultValues);
    }

    public void insertHeight(SQLiteDatabase db, float height, String date){
        ContentValues value = new ContentValues();
        value.put("HEIGHT", height);
        value.put("DATE",date);
        db.insert("HEIGHT", null, value);
    }

    public void insertWeight(SQLiteDatabase db, float weight, String date){
        ContentValues value = new ContentValues();
        value.put("WEIGHT", weight);
        value.put("DATE",date);
        db.insert("WEIGHT", null, value);
    }

    public void insertCurrentTheme (SQLiteDatabase db, int theme){
        ContentValues value = new ContentValues();
        value.put("THEME", theme);
        db.insert("CURRENTTHEME", null, value);
    }

    public void updateCurrentTheme (SQLiteDatabase db, int value){
        ContentValues val = new ContentValues();
        val.put("THEME", value);
        db.update("CURRENTTHEME", val, "_ID = ?", new String[] {"1"});
    }

    public boolean IsProgramCreatedAlready(String name){
        boolean accept = true;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor programsCursor = db.query(TABLE_PROGRAM, null, null, null, null, null, null);

        if (programsCursor != null) {
            if (programsCursor.moveToFirst()) {
                do {
                    if (name.equalsIgnoreCase(programsCursor.getString(programsCursor.getColumnIndex(TITLE)))) {
                        accept = false;
                        break;
                    }
                } while (programsCursor.moveToNext());
            }
            programsCursor.close();
        }

        db.close();

        return accept;
    }

//подсчет количества записей в таблице
    public int getTableLength(String tableName, String condition, int conditionValue){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorLength= db.query(tableName,
                new String[]{ "COUNT(*) AS numb"},
                condition,
                new String[]{Integer.toString(conditionValue)},
                null, null, null);
        int tableLeng = 0;
        if (cursorLength.moveToFirst()) {
            tableLeng = cursorLength.getInt(0);
        }
        cursorLength.close();
        return tableLeng;
    }

    public int getTableLength(String tableName, @Nullable String condition, @Nullable String conditionValue){
        String[] value=null;
        if (conditionValue != null){
           value = new String[]{conditionValue};
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorLength= db.query(tableName,
                new String[]{ "COUNT(*) AS numb"},
                condition,
                value,
                null, null, null);
        int tableLeng = 0;
        if (cursorLength.moveToFirst()) {
            tableLeng = cursorLength.getInt(0);
        }
        cursorLength.close();
        return tableLeng;
    }
}


