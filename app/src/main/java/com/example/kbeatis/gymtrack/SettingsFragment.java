package com.example.kbeatis.gymtrack;


import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_settings));
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.settings_recycler);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        String[] titles = {"Цветовая тема", "Измерения тела", "Язык", "Оценить приложение"};
        SettingsAdapter adapter = new SettingsAdapter(titles);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
        final @ColorInt int color = typedValue.data;

        adapter.setListener(new SettingsAdapter.Listener() {
            @Override
            public void onClick(int position) {
                if (position == 0){
                    new MaterialDialog.Builder(getActivity())
                            .titleColor(color)
                            .positiveColor(color)
                            .neutralColor(color)
                            .negativeColor(color)
                            .title("Смена темы")
                            .items(R.array.themes)
                            .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                @Override
                                public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                    /**
                                     * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                                     * returning false here won't allow the newly selected radio button to actually be selected.
                                     **/
                                    GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(getContext());
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    helper.updateCurrentTheme(db, which);
                                    db.close();
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.putExtra("setting", true);
                                    startActivity(intent);

                                    return true;
                                }
                            })
                            .positiveText(R.string.delete_positive)
                            .show();
                }

                if (position == 1){
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, new BodyParametersFragment(), "visible_fragment");
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    fragmentTransaction.commit();
                }
            }
        });
        return view;
    }

}
