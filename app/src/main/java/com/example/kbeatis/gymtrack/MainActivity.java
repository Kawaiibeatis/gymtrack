package com.example.kbeatis.gymtrack;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    Fragment fragment;
    boolean setting;
    private  int theme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        theme = 0;
        setting = intent.getBooleanExtra("setting", false);
        initialization();
    }

    private  void initialization(){
        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query("CURRENTTHEME",
                new String[] {"THEME"},
                "_ID = ?",
                new String[] {"1"},
                null,null,null);
        if(cursor.moveToFirst()){
            theme = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        setContentView(R.layout.activity_main);
        switch (theme){
            case 0:
                setTheme(R.style.AppTheme);
                break;
            case 1:
                setTheme(R.style.AppTheme2);
                break;
            case 2:
                setTheme(R.style.AppTheme3);
                break;
        }
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = new Bundle();
        bundle.putInt("theme", theme);
        Fragment fragment;
        if (setting){
            fragment = new SettingsFragment();
        }else {
            fragment = new TrainingFragment();
        }
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();

        toolbar.setTitle(String.valueOf(R.string.nav_train));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        setTitle(R.string.app_name);
        setGradient();
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void setGradient(){
        RelativeLayout rel = (RelativeLayout) findViewById(R.id.content_main);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        ShapeDrawable mDrawable = new ShapeDrawable(new RectShape());
        switch (theme){
            case 0:
                mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                        getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccentLight), Shader.TileMode.CLAMP));
                rel.setBackgroundDrawable(mDrawable);
                break;
            case 1:
                mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                        getResources().getColor(R.color.colorPrimary2), getResources().getColor(R.color.colorAccentLight2), Shader.TileMode.CLAMP));
                rel.setBackgroundDrawable(mDrawable);
                break;
            case 2:
                mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                        getResources().getColor(R.color.colorPrimary3), getResources().getColor(R.color.colorAccentLight3), Shader.TileMode.CLAMP));
                rel.setBackgroundDrawable(mDrawable);
                break;
        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        /*
        if (id == R.id.action_colors_1){
            theme=0;
            initialization();
            //recreate();
            //setGradient();
        }

        if (id == R.id.action_colors_2){
            theme = 1;
            initialization();
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()){
            case R.id.nav_train:
                fragment =  new TrainingFragment();
                break;
            case R.id.nav_program:
                fragment = new ProgramFragment();
                break;
            case R.id.nav_graph:
                fragment = new GraphFragment();
                break;
            case R.id.nav_exercise:
                fragment = new ExerciseFragment();
                break;
            case R.id.nav_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.nav_info:
                fragment = new InformationFragment();
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("theme", theme);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();
        toolbar.setTitle(String.valueOf(item));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
