package com.example.kbeatis.gymtrack;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by kbeatis on 13.03.17.
 */

public class PreProgressAdapter extends RecyclerView.Adapter<PreProgressAdapter.ViewHolder> {
    private String[] day_title;
    private String[] exercise_title;
    private Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }

    public void setListener(Listener listener){
        this.listener=listener;
    }

    public PreProgressAdapter(String[] day_title, String[] exercise_title){
        this.day_title = day_title;
        this.exercise_title = exercise_title;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout cardView;

        public ViewHolder(RelativeLayout v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public PreProgressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_preprogress, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(PreProgressAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardView = holder.cardView;
        TextView mDayTitle = (TextView)cardView.findViewById(R.id.daytitle_text);
        mDayTitle.setText(String.valueOf(day_title[position]));
        TextView mStringBuilder = (TextView)cardView.findViewById(R.id.stringbuilder_text);
        mStringBuilder.setText(exercise_title[position]);
        RelativeLayout mRelativeLayoutStart = (RelativeLayout)cardView.findViewById(R.id.start_button_view);

        mRelativeLayoutStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return day_title.length;
    }
}
