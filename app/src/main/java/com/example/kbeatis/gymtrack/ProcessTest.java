package com.example.kbeatis.gymtrack;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.rey.material.widget.Slider;

import java.util.Timer;
import java.util.TimerTask;
import at.markushi.ui.CircleButton;

public class ProcessTest extends AppCompatActivity implements View.OnClickListener{
    TextView mTextViewTimeCounter;
    FrameLayout mButtonRightFL, mButtonLeftFL;
    CircleButton mButtonMain, mButtonLeft, mButtonRight;
    Slider mSlider;
    View mView;
    int counter = 5;
    int weight = 50;
    private Timer timer;
    private Long timeSec;
    private ScaleAnimation scale_text;
    private RotateAnimation rotate;
    boolean isVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mView = (View) findViewById(R.id.circle);
        mView.setVisibility(View.GONE);
        mSlider = (Slider) findViewById(R.id.slider);
        mSlider.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                mTextViewTimeCounter.setText(String.valueOf(mSlider.getValue()));
                weight = mSlider.getValue();
            }
        });
        mButtonRightFL= (FrameLayout)findViewById(R.id.right_button_fl);
        mButtonLeftFL = (FrameLayout)findViewById(R.id.left_button_fl);
        mButtonLeft = (CircleButton)findViewById(R.id.left_button);
        mButtonRight = (CircleButton)findViewById(R.id.right_button);
        mButtonLeft.setOnClickListener(this);
        mButtonRight.setOnClickListener(this);
        mButtonMain = (CircleButton)findViewById(R.id.main_button);
        mTextViewTimeCounter = (TextView) findViewById(R.id.countdown_counter);
        mTextViewTimeCounter.setTextSize(26);
        mTextViewTimeCounter.setText(String.valueOf(weight));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        isVisible = true;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttons_animation(isVisible);
                mView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timeSec = System.currentTimeMillis();
                        startTimer();
                    }
                },1000);

            }
        });
        mButtonMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isVisible){
                    buttons_animation(isVisible);
                }
            }
        });
        rotate = (RotateAnimation) AnimationUtils.loadAnimation(ProcessTest.this, R.anim.rotate_view);
        scale_text = (ScaleAnimation) AnimationUtils.loadAnimation(ProcessTest.this, R.anim.scale_text_timer);
    }

    void buttons_animation(boolean isVisible) {
        final Animation mSliderFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        final Animation mSlideFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        final Animation mButtonMainAnimation = AnimationUtils.loadAnimation(this,R.anim.scale_start_button);
        final Animation mRightButtonOut = AnimationUtils.loadAnimation(this,R.anim.right_button_out);
        final Animation mLeftButtonOut = AnimationUtils.loadAnimation(this,R.anim.left_button_out);
        final Animation mLeftButtonIn = AnimationUtils.loadAnimation(this,R.anim.left_button_in);
        final Animation mRightButtonIn = AnimationUtils.loadAnimation(this,R.anim.right_button_in);

        if (isVisible){
            mView.setVisibility(View.VISIBLE);
            mView.startAnimation(mSlideFadeOut);
            mSlider.startAnimation(mSliderFadeIn);
            mButtonLeftFL.startAnimation(mLeftButtonOut);
            mButtonLeftFL.setVisibility(View.GONE);
            mButtonMain.startAnimation(mButtonMainAnimation);
            mButtonRightFL.startAnimation(mRightButtonOut);
            mButtonRightFL.setVisibility(View.GONE);
        } else {
            mTextViewTimeCounter.startAnimation(mSliderFadeIn);
            mSlider.startAnimation(mSlideFadeOut);
            mSlider.setVisibility(View.VISIBLE);
            mButtonMain.startAnimation(mButtonMainAnimation);
            mButtonLeftFL.startAnimation(mLeftButtonIn);
            mButtonLeftFL.setVisibility(View.VISIBLE);
            mButtonRightFL.startAnimation(mRightButtonIn);
            mButtonRightFL.setVisibility(View.VISIBLE);

        }
        this.isVisible = !isVisible;
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (timeSec == null) {
                    if (timer != null) timer.cancel();
                    return;
                }
                final long sec = counter - (System.currentTimeMillis() - timeSec) / 1000;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (sec > 0) {
                            mTextViewTimeCounter.startAnimation(scale_text);
                            mTextViewTimeCounter.setText(String.valueOf(sec));
                            findViewById(R.id.circletime_view).startAnimation(rotate);
                        } else {
                            timer.cancel();
                            mTextViewTimeCounter.setText(String.valueOf(0));
                            timeSec = null;
                            ObjectAnimator mCounterFadeOut = ObjectAnimator.ofFloat(mTextViewTimeCounter, View.ALPHA, 0, 1);
                            mCounterFadeOut.setDuration(1000);
                            mCounterFadeOut.start();
                            mTextViewTimeCounter.setTextSize(25);
                            mTextViewTimeCounter.setText("Начать\nподход");
                        }
                    }
                });
            }
        },0, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null)
            timer.cancel();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.left_button:
                weight= weight - 1;
                break;
            case R.id.right_button:
                weight= weight + 1;
                break;
        }
        mTextViewTimeCounter.setText(String.valueOf(weight));
    }
}

//    public class CountThread extends Thread{
//        public void run(){
//            for (int i = 0; i < counter*360; i++) {
//                final int finalI = i;
//                mView.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        currentX = Float.valueOf(mView.getX()) + radius * Math.cos(finalI * Math.PI / 180);
//                        currentY = Float.valueOf(mView.getY()) + radius * Math.sin(finalI * Math.PI / 180);
//                        mView.setY((float) currentY);
//                        mView.setX((float) currentX);
//                    }
//                }, (long) (magic_number*i));
//            }
//        }
//    }
//
//    private void animation_set(){
//
//    }
//
//
//    void countdown_timer() {
//        for (int i = 0; i < counter; i++) {
//            mTextViewTimeCounter.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    counter_test = counter_test - 1;
//                    mTextViewTimeCounter.setText(String.valueOf(counter_test));
//                }
//            }, 1000 * i);
//        }
//    }
//}
