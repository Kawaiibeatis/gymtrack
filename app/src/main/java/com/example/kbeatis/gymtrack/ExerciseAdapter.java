package com.example.kbeatis.gymtrack;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by kbeatis on 06.01.17.
 */

class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {

    private int[] title;
    private int[] imageId;
    private ExerciseAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }


    public void setListener(ExerciseAdapter.Listener listener){
        this.listener=listener;
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        public ViewHolder(CardView view) {
            super(view);
            cardView = view;
        }
    }

    public  ExerciseAdapter(int[] title, int[] imagePosition){
        this.title = title;
        this.imageId = imagePosition;
    }

    @Override
    public ExerciseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_exercise, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ExerciseAdapter.ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        ImageView imageView = (ImageView)cardView.findViewById(R.id.exercise_image);
        imageView.setImageResource(imageId[position]);
        TextView textView = (TextView)cardView.findViewById(R.id.exercise_text);
        textView.setText(title[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return title.length;
    }
}




























