package com.example.kbeatis.gymtrack;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Konstantin on 10.03.2017.
 */

public class GraphExerciseAdapter extends RecyclerView.Adapter<GraphExerciseAdapter.ViewHolder> {


    private int[] title;
    private int[] imageId;
    private GraphExerciseAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }


    public void setListener(GraphExerciseAdapter.Listener listener){
        this.listener=listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    public GraphExerciseAdapter(int[] title, int[] imagePosition){
        this.title = title;
        this.imageId = imagePosition;
    }

    @Override
    public GraphExerciseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_graph, parent, false);
        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(GraphExerciseAdapter.ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        ImageView imageView = (ImageView)cardView.findViewById(R.id.graph_image);
        imageView.setImageResource(imageId[position]);
        TextView textView = (TextView)cardView.findViewById(R.id.graph_text);
        textView.setText(title[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return title.length;
    }
}
