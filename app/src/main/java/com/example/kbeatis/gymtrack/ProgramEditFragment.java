package com.example.kbeatis.gymtrack;


import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProgramEditFragment extends Fragment {

    int tableLength;
    int [] titles;
    int id_day;
    int[] titlesExercise;
    int[] id_exercise;
    int[] id_exerciseAll;
    RecyclerView recyclerView;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_program));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_program_edit, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        id_day = getArguments().getInt("id_day");
        mainProgram();

        return recyclerView;
    }

    private void mainProgram(){
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("MUSCLE", null, null);
            titles = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();

            Cursor cursor= db.query("MUSCLE",
                    new String[]{ "TITLE"},
                    null,
                    null,
                    null, null, null);
            if (cursor.moveToFirst()) {
                titles[0] = cursor.getInt(0);
            }
            for (int i = 1;i < tableLength+1; i++) {
                if (cursor.moveToNext()) {
                    titles[i] = cursor.getInt(0);
                }
            }
            cursor.close();
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        //Получение списка используемых упражнений для данного дня
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("exday", "id_day = ?", id_day);
            id_exercise = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("exday",
                    new String[]{"id_exercise"},
                    "id_day = ?",
                    new String[]{Integer.toString(id_day)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                id_exercise[0] = cursor.getInt(0);
            }
            for (int i = 1; i < tableLength + 1; i++) {
                if (cursor.moveToNext()) {
                    id_exercise[i] = cursor.getInt(0);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        ProgramEditAdapter adapter = new ProgramEditAdapter(getActivity(), titles, getNumberExercises());
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter.setListener(new ProgramEditAdapter.Listener() {
            @Override
            public void onClick(int position) {
                position++;
                // Получение названия и id упражнений для вывода в диалог
                try {
                    GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                    tableLength = gymTrackDatabaseHelper.getTableLength("Exercise", "id_muscle = ?", position);
                    titlesExercise = new int[tableLength];
                    id_exerciseAll = new int[tableLength];
                    SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
                    Cursor cursor = db.query("exercise",
                            new String[]{"title", "id_exercise"},
                            "id_muscle = ?",
                            new String[]{Integer.toString(position)},
                            null, null, null);
                    if (cursor.moveToFirst()) {
                        titlesExercise[0] = cursor.getInt(0);
                        id_exerciseAll[0] = cursor.getInt(1);
                    }
                    for (int i = 1; i < tableLength + 1; i++) {
                        if (cursor.moveToNext()) {
                            titlesExercise[i] = cursor.getInt(0);
                            id_exerciseAll[i] = cursor.getInt(1);
                        }
                    }
                    cursor.close();
                    db.close();
                } catch (SQLiteException e){
                    Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
                    toast.show();
                }

                TypedValue typedValue = new TypedValue();
                Resources.Theme theme = getContext().getTheme();
                theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
                final @ColorInt int color = typedValue.data;

                final Integer[] check =checkBitch(id_exerciseAll, id_exercise);
                new MaterialDialog.Builder(getActivity())
                        .titleColor(color)
                        .positiveColor(color)
                        .neutralColor(color)
                        .negativeColor(color)
                        .title(R.string.exercises)
                        .items(myGetString(titlesExercise))
                        .itemsCallbackMultiChoice(check, new MaterialDialog.ListCallbackMultiChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                                GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(getActivity());
                                SQLiteDatabase db = helper.getWritableDatabase();
                                Integer[] conflict = new Integer[id_exerciseAll.length];
                                Integer[] toDelete = new Integer[id_exerciseAll.length];
                                boolean triger;
                                for (int i =0; i<id_exerciseAll.length;i++){
                                    conflict[i] = -1;
                                    toDelete[i] = -1;
                                }
                                for (int i = 0; i<which.length;i++){
                                    triger=true;
                                    for (int j = 0; j<check.length;j++){
                                        if (which[i]==check[j]){
                                            triger=false;
                                        }
                                    }
                                    if(triger){
                                        conflict[i]=which[i];
                                    } else {
                                        conflict[i]=-1;
                                    }
                                }

                                for (int i = 0; i<check.length;i++){
                                    triger=true;
                                    for (int j = 0; j<which.length;j++){
                                        if (which[j]==check[i]){
                                            triger=false;
                                        }
                                    }
                                    if(triger){
                                        toDelete[i]=check[i];
                                    } else {
                                        toDelete[i]=-1;
                                    }
                                }
                                for (int i = 0;i < id_exerciseAll.length;i++){
                                    for (int j = 0; j<id_exerciseAll.length;j++){
                                        if (i == conflict[j]){
                                            helper.insertExInDay(db, id_exerciseAll[i], id_day);
                                        }
                                        if (i == toDelete[j]){
                                            helper.deleteExInDay(db, id_exerciseAll[i]);
                                        }
                                    }
                                }
                                db.close();
                                mainProgram();
                                return true;
                            }
                        })
                        .positiveText("Готово")
                        .show();

            }
        });
    }
    private String[] myGetString(int [] values){
        String[] result = new String[values.length];
        for (int i = 0; i<values.length;i++){
            result[i] = getString(values[i]);
        }
        return result;
    }
    //возвращает массив с упражнениями, которые есть в бд, используется для заполнения диалога
    private Integer[] checkBitch(int[] big, int[] small){
        List<Integer> usingExercisesList = new ArrayList<>();
        for (int i = 0; i < big.length; i++){
            for (int j = 0;j<small.length;j++){
                if (big[i] == small[j]){
                    usingExercisesList.add(i);
                }
            }
        }
        return usingExercisesList.toArray(new Integer[usingExercisesList.size()]);
    }
    private int[] getNumberExercises(){
        int[] idExercise = new int[0];
        int[] idMuscle = {0,0,0,0,0,0,0,0,0,0,0};
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            int leng = gymTrackDatabaseHelper.getTableLength("exday", "id_day = ?", id_day);
            idExercise = new int[leng];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("exday",
                    new String[]{"id_exercise"},
                    "id_day = ?",
                    new String[]{Integer.toString(id_day)},
                    null, null, null);
            if (cursor.moveToFirst()) {
                idExercise[0] = cursor.getInt(0);
            }
            for (int i = 1; i < leng + 1; i++) {
                if (cursor.moveToNext()) {
                    idExercise[i] = cursor.getInt(0);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        try {
            for (int i = 0;i < idExercise.length;i++){
                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                int leng = gymTrackDatabaseHelper.getTableLength("exercise", "id_exercise = ?", idExercise[i]);
                SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
                Cursor cursor = db.query("exercise",
                        new String[]{"id_muscle"},
                        "id_exercise = ?",
                        new String[]{Integer.toString(idExercise[i])},
                        null, null, null);
                if (cursor.moveToFirst()) {
                    idMuscle[cursor.getInt(0)-1]++;
                }
                cursor.close();
                db.close();
            }
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return idMuscle;
    }
}
