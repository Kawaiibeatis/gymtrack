package com.example.kbeatis.gymtrack;


import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.kbeatis.gymtrack.R;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingFragment extends Fragment {
    View view;
    int theme;
    private int seeker, weight, repeats, id_exercise, exercise_title;
    private RecyclerView recyclerView;
    private String[] id_program, id_day, program_title, day_title, date, exercises;
    private int[] id_result;
    private StringBuilder stringBuilder;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createArray();
    }

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_train));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        theme = getArguments().getInt("theme", 0);
        view = inflater.inflate(R.layout.fragment_training, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.train_recycler);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        setRecyclerView();
        FloatingActionButton fab = (FloatingActionButton)view.findViewById(R.id.fab_training);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PreProgressActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        createArray();
//        setRecyclerView();
//    }

    private void setRecyclerView(){
        TrainingAdapter adapter = new TrainingAdapter(program_title, day_title, date);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
        final @ColorInt int color = typedValue.data;

        adapter.setListener(new TrainingAdapter.Listener() {
            @Override
            public void onClick(final int position) {
                MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                        .titleColor(color)
                        .positiveColor(color)
                        .neutralColor(color)
                        .negativeColor(color)
                        .title(day_title[position])
                        .customView(R.layout.dialog_more_exercise, true)
                        .positiveText(R.string.dialog_done)
                        .build();

                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
                SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
                Cursor cursorProgress = db.query("progress", new String[]{"id_progress"}, null, null, null, null,null);
                if (cursorProgress.moveToLast()) seeker = cursorProgress.getInt(0);
                seeker = seeker - position;
                id_result = new int[gymTrackDatabaseHelper.getTableLength("progressresult","id_progress = ?", seeker)];
                Cursor cursorProgressResult = db.query("progressresult", new String[]{"id_result"},"id_progress = ?" , new String[]{Integer.toString(seeker)}, null, null, null);
                if (cursorProgressResult.moveToFirst()) id_result[0] = cursorProgressResult.getInt(0);
                for (int i = 1; i < id_result.length; i++) if (cursorProgressResult.moveToNext()) id_result[i] = cursorProgressResult.getInt(0);
                cursorProgressResult.close();
                //////////////////////////////////
                exercises = new String[id_result.length];
                for (int i = 0; i < id_result.length; i++){
                    Cursor cursorResult = db.query("result", new String[]{"id_exercise", "weight", "repeats"}, "id_result = ?", new String[]{Integer.toString(id_result[i])}, null, null, null);
                    if (cursorResult.moveToLast()) {
                        id_exercise = cursorResult.getInt(0);
                        weight = cursorResult.getInt(1);
                        repeats = cursorResult.getInt(2);
                    }
                    Cursor cursorExercise = db.query("exercise", new String[]{"title"}, "id_exercise = ?", new String[]{Integer.toString(id_exercise)}, null, null, null);
                    if (cursorExercise.moveToFirst()) exercise_title = cursorExercise.getInt(0);
                    cursorExercise.close();
                    stringBuilder = new StringBuilder();
                    String string = getString(exercise_title);
                    if (i == 0){
                        stringBuilder.append(" ").append(string).append(": ").append(weight).append("x").append(repeats).append("\n");
                    } else {
                        stringBuilder.append(string).append(": ").append(weight).append("x").append(repeats).append("\n");;
                    }
                    exercises[i] = stringBuilder.toString();
                }
                db.close();
                final View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
                TextView mTextViewDay = (TextView) dialog.getCustomView().findViewById(R.id.dayTitle);
                mTextViewDay.setText(Arrays.toString(exercises).replaceAll("\\[|\\]|,",""));
                dialog.show();
                positiveAction.setEnabled(true);
            }
        });
    }

    private void createArray(){
        GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        Cursor cursorProgress;
//        id_program = new int[gymTrackDatabaseHelper.getTableLength("progress", null, null)];
//        id_day = new int[gymTrackDatabaseHelper.getTableLength("progress", null, null)];
//        date = new int[gymTrackDatabaseHelper.getTableLength("progress", null, null)];
        id_program = new String[getTableLength("progress")];
        id_day = new String[getTableLength("progress")];
        date = new String[getTableLength("progress")];
        cursorProgress = db.query("progress", new String[]{"id_program", "id_day", "date"}, null, null, null, null,null);
        if (cursorProgress.moveToLast()){
            id_program[0] = cursorProgress.getString(0);
            id_day[0] = cursorProgress.getString(1);
            date[0] = cursorProgress.getString(2);
        }
        for (int i = 1; i < id_program.length; i++){
            if (cursorProgress.moveToPrevious()){
                id_program[i] = cursorProgress.getString(0);
                id_day[i] = cursorProgress.getString(1);
                date[i] = cursorProgress.getString(2);
            }
        }
        cursorProgress.close();
        Cursor cursorProgram;
        program_title = new String[id_program.length];
        for (int i = 0; i < program_title.length; i++){
            cursorProgram = db.query("program", new String[]{"title"}, "id_program = ?", new String[]{id_program[i]}, null, null, null);
            if (cursorProgram.moveToNext()) program_title[i] = cursorProgram.getString(0);
            cursorProgram.close();
        }
        Cursor cursorDay;
        day_title = new String[id_day.length];
        for (int i = 0; i < day_title.length; i++){
            cursorDay = db.query("day", new String[]{"title"}, "id_day = ?", new String[]{id_day[i]}, null, null, null);
            if (cursorDay.moveToNext()) day_title[i] = cursorDay.getString(0);
            cursorDay.close();
            }

        db.close();
    }

    public int getTableLength(String tableName){
        SQLiteOpenHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        Cursor cursorLength= db.query(tableName,
                new String[]{ "COUNT(*) AS numb"},
                null,
                null,
                null, null, null);
        int tableLeng = 0;
        if (cursorLength.moveToFirst()) {
            tableLeng = cursorLength.getInt(0);
        }
        cursorLength.close();
        db.close();
        return tableLeng;
    }

}
