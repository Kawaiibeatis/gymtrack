package com.example.kbeatis.gymtrack;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class PreProgressActivity extends AppCompatActivity {
    int theme = 0;
    private int search;
    private int[] id_day;
    private int tableLength;
    private String[] titles;
    private String[] id_program;
    private int[] day;
    private int[] exercise;
    private String[] day_title;
    private int[] exercise_title;
    private StringBuilder stringBuilder;
    private RecyclerView preprogressRecycler;
    private Spinner spinner;
    public static final String ID_DAY = "com.gymtrack.ID_DAY";
    public static final String ID_PROGRAM = "com.gymtrack.ID_PROGRAM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialization();
    }


    private void initialization(){
        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query("CURRENTTHEME",
                new String[] {"THEME"},
                "_ID = ?",
                new String[] {"1"},
                null,null,null);
        if(cursor.moveToFirst()){
            theme = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        setContentView(R.layout.activity_pre_progress);
        switch (theme){
            case 0:
                setTheme(R.style.AppTheme);
                break;
            case 1:
                setTheme(R.style.AppTheme2);
                break;
            case 2:
                setTheme(R.style.AppTheme3);
                break;
        }
        setContentView(R.layout.activity_pre_progress);
        getProgram();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setGradient();
        ArrayAdapter<CharSequence> spinnerAdapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_item, id_program);
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, id_program);
//        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.setDropDownViewResource(R.layout.dropdownitem);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setRecycler(i+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setSupportActionBar(toolbar);
    }

    private String[] getProgram(){
        GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        tableLength = gymTrackDatabaseHelper.getTableLength("program",null, null);
        id_program = new String[tableLength];
        Cursor cursor;
        cursor = db.query("program", new String[]{"title"}, null, null, null, null, null);
        if (cursor.moveToFirst()) id_program[0] = cursor.getString(0);
        for (int i = 1; i < id_program.length; i++){
            if (cursor.moveToNext()) id_program[i] = cursor.getString(0);
        }
        cursor.close();
        db.close();
        return id_program;
    }

    private void setRecycler(final int id_program){
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursorInProgram;
            tableLength = getTableLength("DAY", "ID_PROGRAM", String.valueOf(id_program));
            day = new int[tableLength];
            cursorInProgram = db.query("DAY",
                    new String[]{"ID_DAY"},
                    "ID_PROGRAM = ?",
                    new String[]{Integer.toString(id_program)},
                    null, null, null
                    );
            if (cursorInProgram.moveToFirst()){
                day[0] = cursorInProgram.getInt(0);
            }
            for (int i = 1; i < day.length; i++){
                if (cursorInProgram.moveToNext()){
                    day[i] = cursorInProgram.getInt(0);
                }
            }
            cursorInProgram.close();

            Cursor cursorDay;
            day_title = new String[day.length];
            for (int i = 0; i < day_title.length; i++){
                search = day[i];
                cursorDay = db.query("DAY",
                        new String[]{"TITLE"},
                        "ID_DAY = ?",
                        new String[]{Integer.toString(search)},
                        null, null, null
                );
                if (cursorDay.moveToNext()){
                    day_title[i] = cursorDay.getString(0);
                }
                cursorDay.close();
            }

            Cursor cursorInDay;
            Cursor cursorExercise;
            titles = new String[day.length];
            for (int i = 0; i < day.length; i++){
                search = day[i];
                exercise = new int[getTableLength("EXDAY", "ID_DAY", String.valueOf(search))];
                cursorInDay = db.query("EXDAY", new String[]{"ID_EXERCISE"}, "ID_DAY = ?", new String[]{Integer.toString(search)}, null, null, null);
                for (int j = 0; j < exercise.length; j++){
                    if (cursorInDay.moveToNext()) exercise[j] = cursorInDay.getInt(0);
                }
                cursorInDay.close();
                exercise_title = new int[exercise.length];
                for (int k = 0; k < exercise.length; k++){
                    search = exercise[k];
                    cursorExercise = db.query("EXERCISE", new String[]{"TITLE"}, "ID_EXERCISE = ?", new String[]{Integer.toString(search)}, null, null, null);
                    if (cursorExercise.moveToNext()){
                        exercise_title[k] = cursorExercise.getInt(0);
                    }
                    cursorExercise.close();
                }
                stringBuilder = new StringBuilder();
                for (int l = 0; l < exercise_title.length; l++){
                    String string = getString(exercise_title[l]);
                    stringBuilder.append(string).append("\n");
                }
                titles[i] = stringBuilder.toString();
            }
//            inClause = exercise.toString();
//            inClause = inClause.replace("[","(");
//            inClause = inClause.replace("]",")");
            db.close();
        } catch (SQLiteException e){
            Toast.makeText(getApplicationContext(), "Database unavailable", Toast.LENGTH_SHORT).show();
        }
        preprogressRecycler = (RecyclerView) findViewById(R.id.pregrogress_recycler);
        PreProgressAdapter adapter = new PreProgressAdapter(day_title, titles);
        preprogressRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        preprogressRecycler.setLayoutManager(layoutManager);
        adapter.setListener(new PreProgressAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(PreProgressActivity.this, AlternativeProgress.class);
                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getApplication());
                SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
                Cursor cursor;
                id_day = new int[getTableLength("day","id_program", String.valueOf(id_program))];
                cursor = db.query("day",new String[]{"id_day"},"id_program = ?", new String[]{Integer.toString(id_program)},null,null, null);
                if (cursor.moveToFirst()) {
                    id_day[0] = cursor.getInt(0);
                }
                for (int i = 1; i < id_day.length; i++)
                    if (cursor.moveToNext()) {
                        id_day[i] = cursor.getInt(0);
                }
                cursor.close();
                int[] exception = new int[getTableLength("exday", "id_day", String.valueOf(id_day[position]))];
                if (exception.length == 0){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.exception), Toast.LENGTH_SHORT).show();
                }
                else {
                    db.close();
                    intent.putExtra(ID_DAY, id_day[position]);
                    intent.putExtra(ID_PROGRAM, id_program+1);
                    startActivity(intent);
                }

            }
        });
    }

    public int getTableLength(String tableName, String whereName, String searchName){
        SQLiteOpenHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        Cursor cursorLength= db.query(tableName,
                new String[]{ "COUNT(*) AS numb"},
                whereName +" = ?",
                new String[]{Integer.toString(Integer.parseInt(searchName))},
                null, null, null);
        int tableLeng = 0;
        if (cursorLength.moveToFirst()) {
            tableLeng = cursorLength.getInt(0);
        }
        cursorLength.close();
        db.close();
        return tableLeng;
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_colors_1){
            theme=0;
            initialization();
            //recreate();
            //setGradient();
        }

        if (id == R.id.action_colors_2){
            theme = 1;
            initialization();
        }

        return super.onOptionsItemSelected(item);
    }
*/
private void setGradient(){
    RelativeLayout rel = (RelativeLayout) findViewById(R.id.content_main);
    Display display = getWindowManager().getDefaultDisplay();
    int width = display.getWidth();
    int height = display.getHeight();
    ShapeDrawable mDrawable = new ShapeDrawable(new RectShape());
    switch (theme){
        case 0:
            mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                    getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorAccentLight), Shader.TileMode.CLAMP));
            rel.setBackgroundDrawable(mDrawable);
            break;
        case 1:
            mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                    getResources().getColor(R.color.colorPrimary2), getResources().getColor(R.color.colorAccentLight2), Shader.TileMode.CLAMP));
            rel.setBackgroundDrawable(mDrawable);
            break;
        case 2:
            mDrawable.getPaint().setShader(new LinearGradient(0, 0, width, height,
                    getResources().getColor(R.color.colorPrimary3), getResources().getColor(R.color.colorAccentLight3), Shader.TileMode.CLAMP));
            rel.setBackgroundDrawable(mDrawable);
            break;
    }
}
}
