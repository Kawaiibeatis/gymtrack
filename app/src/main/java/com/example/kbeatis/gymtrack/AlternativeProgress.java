package com.example.kbeatis.gymtrack;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rey.material.widget.Slider;

import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;

import at.markushi.ui.CircleButton;

public class AlternativeProgress extends Activity implements View.OnClickListener {
    private long sec;
    private Timer timer;
    private Long timeSec;
    private String programTitle; int description; int id_image_src;
    private int tableLength, positionInArray, searchInArray;
    private int weight, approach = 1, repeats;
    private int bigPosition;
    private int id_day, id_progress, id_result, counter;
    private int[] id;
    private int[] titles;
    private CardView cardView;
    private RecyclerView recyclerView;
    private EditText mWeightEdit, mRepeatEdit, mTimeEdit;
    private TextView mProgramTitle, mExerciseTitle, mApproach, mCounter;
    private RelativeLayout mButtonApproach, mButtonExercise;
    private FrameLayout view, timer_layout;
    ScaleAnimation scale_text;
    RotateAnimation rotate;
    CircleButton mButtonMain;
    public static final String ID_DAY = "com.gymtrack.ID_DAY";
    public static final String ID_PROGRAM = "com.gymtrack.ID_PROGRAM";
    int theme = 0;

    boolean timerIsWorking = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_alternative_progress);
        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query("CURRENTTHEME",
                new String[] {"THEME"},
                "_ID = ?",
                new String[] {"1"},
                null,null,null);
        if(cursor.moveToFirst()){
            theme = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        switch (theme){
            case 0:
                setTheme(R.style.AppTheme);
                break;
            case 1:
                setTheme(R.style.AppTheme2);
                break;
            case 2:
                setTheme(R.style.AppTheme3);
                break;
        }
        setContentView(R.layout.activity_alternative_progress);
        cardView = (CardView) findViewById(R.id.cardViewAnimation);
        mCounter = (TextView)findViewById(R.id.countdown_counter);
        switch (theme){
            case 0:
                cardView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mCounter.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case 1:
                cardView.setBackgroundColor(getResources().getColor(R.color.colorPrimary2));
                mCounter.setTextColor(getResources().getColor(R.color.colorPrimary2));
                break;
            case 2:
                cardView.setBackgroundColor(getResources().getColor(R.color.colorPrimary3));
                mCounter.setTextColor(getResources().getColor(R.color.colorPrimary3));
                break;
        }
        /////////////////////////////////////////////////////////инициализация
        timer_layout = (FrameLayout) findViewById(R.id.buttons_layout);
        view = (FrameLayout) findViewById(R.id.circletime_view);
        mProgramTitle = (TextView)findViewById(R.id.textViewProgramName);
        mButtonMain = (CircleButton)findViewById(R.id.main_button);
        mExerciseTitle = (TextView)findViewById(R.id.textViewExerciseName);
        mApproach = (TextView)findViewById(R.id.textViewApproach);

        mButtonApproach = (RelativeLayout)findViewById(R.id.rlButtonApproach);
        mButtonExercise = (RelativeLayout)findViewById(R.id.rlButtonExecise);
        mWeightEdit = (EditText)findViewById(R.id.editTextWeight);
        mRepeatEdit = (EditText)findViewById(R.id.editTextRepeat);
        mTimeEdit = (EditText)findViewById(R.id.editTextTime);
//        mSlider = (Slider)findViewById(R.id.slider);
        /////////////////////////////////////////////////////////////////////
        getTitleProgram();
        createArray();
        setRecyclerView(true);
        getPositionInArray();
        setProgress();
        mExerciseTitle.setText(getString(titles[0]));
        view.setOnClickListener(this);
        mButtonApproach.setOnClickListener(this);
        mButtonExercise.setOnClickListener(this);
        mProgramTitle.setText(programTitle);

        rotate = (RotateAnimation) AnimationUtils.loadAnimation(this, R.anim.rotate_view);
        scale_text = (ScaleAnimation) AnimationUtils.loadAnimation(this, R.anim.scale_text_timer);
    }



    private String getTitleProgram(){
        Intent intent = getIntent();
        int programID = intent.getIntExtra(ID_PROGRAM, 0);
        GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        Cursor cursorProgramTitle;
        cursorProgramTitle = db.query("program", new String[]{"title"}, "id_program = ?", new String[]{Integer.toString(programID - 1)}, null, null, null);
        if (cursorProgramTitle.moveToFirst()) programTitle = cursorProgramTitle.getString(0);
        cursorProgramTitle.close();
        db.close();
        return programTitle;
    }

    private int[] createArray(){
        id_day = getIntent().getIntExtra(ID_DAY, 0);
        tableLength = getTableLength("EXDAY", "ID_DAY", id_day);
        id = new int[tableLength];
        try {
            SQLiteOpenHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursorExDay;
            cursorExDay = db.query("EXDAY",
                    new String[]{"ID_EXERCISE"},
                    "ID_DAY = ?",
                    new String[]{Integer.toString(id_day)},
                    null, null, null
            );
            if (cursorExDay.moveToFirst()){
                id[0]= cursorExDay.getInt(0);
            }
            for (int i = 1; i < tableLength; i++){
                if (cursorExDay.moveToNext()){
                    id[i]= cursorExDay.getInt(0);
                }
            }
            cursorExDay.close();
            titles = new int[tableLength];
            Cursor cursorExercise;
            for (int i = 0; i < id.length; i++){
                int search = id[i];
                cursorExercise = db.query("EXERCISE",
                        new String[]{"TITLE"},
                        "ID_EXERCISE = ?",
                        new String[]{Integer.toString(search)},
                        null, null, null
                );
                if (cursorExercise.moveToNext()){
                    titles[i] = cursorExercise.getInt(0);
                }
                cursorExercise.close();
            }
            db.close();
        } catch (SQLiteException e){
            Toast.makeText(getApplicationContext(), "Database unavailable", Toast.LENGTH_SHORT).show();
        }
        return titles;
    }

    private void setProgress(){
        GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = gymTrackDatabaseHelper.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String date = String.valueOf(calendar.get(Calendar.YEAR)+ "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.DATE));
        Intent intent = getIntent();
        int id_program = intent.getIntExtra(ID_PROGRAM, 0);
        int id_day = intent.getIntExtra(ID_DAY, 0);
        gymTrackDatabaseHelper.insertProgress(db, id_program-1, id_day, date);
        Cursor cursorProgress;
        cursorProgress = db.query("progress", new String[]{"id_progress"}, null, null, null, null,null);
        if (cursorProgress.moveToLast()) id_progress = cursorProgress.getInt(0);
        cursorProgress.close();
        db.close();
    }

    private void biasInArray(){
        List<Integer> listID = new ArrayList<>();
        List<Integer> listTitles = new ArrayList();
        for (int i = 0; i < id.length; i++){
            listID.add(id[i]);
            listTitles.add(titles[i]);
        }
        listTitles.remove(positionInArray);
        listID.remove(positionInArray);
        id = new  int[listID.size()];
        titles = new int[listTitles.size()];
        for(int i = 0; i < listID.size(); i++) {
            if (listID.get(i) != null ) {
                titles[i] = listTitles.get(i);
                id[i] = listID.get(i);
            }
        }
    }

    private int setRecyclerView(boolean isEnabled){
        recyclerView = (RecyclerView) findViewById(R.id.recyclerProgress);
        AlternativeProgressAdapter adapter = new AlternativeProgressAdapter(titles);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter.setEnable(isEnabled);
        adapter.setListener(new AlternativeProgressAdapter.Listener() {
            @Override
            public void onClick(int position) {
                bigPosition=position;
                mExerciseTitle.setText(getString(titles[position]));
                positionInArray = position;
                approach = 1;
//                mExerciseTitle.setText(Integer.toString(positionInArray));

            }

            @Override
            public void onClickHelp(int position) {
                //Toast.makeText(AlternativeProgress.this, "You touch " + position + " postion", Toast.LENGTH_SHORT).show();
                int helpPosition = id[position];
                MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                        .title(getString(titles[position]))
                        .customView(R.layout.dialog_exercise_help,true)
                        .build();
                ImageView imageView = (ImageView) dialog.getCustomView().findViewById(R.id.imageView);
                TextView textView = (TextView) dialog.getCustomView().findViewById(R.id.textView);
                GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getApplicationContext());
                SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
                Cursor cursor = db.query("exercise",new String[]{"id_image_resource, description"},"id_exercise = ?", new String[]{String.valueOf(helpPosition)},null,null,null);
                if (cursor.moveToFirst()){
                    id_image_src = cursor.getInt(0);
                    description = cursor.getInt(1);
                }
                imageView.setImageResource(id_image_src);
                textView.setText(getString(description));
                dialog.show();
            }

        });
        return positionInArray;
    }


    private int getPositionInArray(){
        searchInArray = id[positionInArray];
        return searchInArray;
    }

    private void setAnimation(){
        final Animation mFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in_from_top);
        final Animation mFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out_to_top);

//            mRepeatEdit.startAnimation(mFadeIn);
//            mWeightEdit.startAnimation(mFadeIn);
//            mTimeEdit.startAnimation(mFadeIn);
            ObjectAnimator fadeInAnimationAlphaWeight = ObjectAnimator.ofFloat(mWeightEdit, View.ALPHA, 0).setDuration(800);
            ObjectAnimator fadeInAnimationTranslateWeight = ObjectAnimator.ofFloat(mWeightEdit, View.TRANSLATION_X, -500f).setDuration(800);
            ObjectAnimator fadeInAnimationAlphaRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.ALPHA, 0).setDuration(800);
            ObjectAnimator fadeInAnimationTranslateRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.TRANSLATION_X, -500f).setDuration(800);
            ObjectAnimator fadeInAnimationAlphaTime = ObjectAnimator.ofFloat(mTimeEdit, View.ALPHA, 0).setDuration(800);
            ObjectAnimator fadeInAnimationTranslateTime = ObjectAnimator.ofFloat(mTimeEdit, View.TRANSLATION_X, -500f).setDuration(800);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(fadeInAnimationAlphaWeight, fadeInAnimationTranslateWeight, fadeInAnimationAlphaRepeat, fadeInAnimationTranslateRepeat, fadeInAnimationAlphaTime, fadeInAnimationTranslateTime);
            animatorSet.start();
//            fadeInAnimationTranslate.setDuration(800)
//            ObjectAnimator fadeOutAnimationAlpha = ObjectAnimator.ofFloat();
//            ObjectAnimator fadeOutAnimationTranslate = ObjectAnimator.ofFloat();

//            mSlider.setVisibility(View.VISIBLE);
//            mSlider.setValueRange(0, Integer.parseInt(mTimeEdit.getText().toString()), true);
//            mSlider.setValue(mSlider.getMaxValue(), true);
//            mCounter.setText(String.valueOf(mSlider.getValue()));
    }

    private void setClickable(boolean isClickable){
//        mSlider.setEnabled(isClickable);
        mButtonExercise.setClickable(isClickable);
        mButtonApproach.setClickable(isClickable);
        mWeightEdit.setEnabled(isClickable);
        mRepeatEdit.setEnabled(isClickable);
        mTimeEdit.setEnabled(isClickable);
        mButtonMain.setEnabled(isClickable);
        setRecyclerView(isClickable);
    }

    private void setVisibility(boolean isVisible){
        if (isVisible){
            mCounter.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            mButtonMain.setVisibility(View.VISIBLE);
        } else {
            mCounter.setVisibility(View.INVISIBLE);
            view.setVisibility(View.INVISIBLE);
            mButtonMain.setVisibility(View.INVISIBLE);
        }
    }

    public void startTimer() {
        timer = new Timer();
        setClickable(false);
        setVisibility(true);
        mExerciseTitle.setText(R.string.rest_time);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (timeSec == null) {
                    if (timer != null) timer.cancel();
                    return;
                }
                sec = counter - (System.currentTimeMillis() - timeSec) / 1000;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (sec > 0) {
                            timerIsWorking = true;
                            mCounter.setText(String.valueOf(sec));
                            mCounter.startAnimation(scale_text);
                            findViewById(R.id.circletime_view).startAnimation(rotate);
                            ///////////////////////////
                            //Notification

                            Date date = new Date(sec*1000);
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
                            String dateText = simpleDateFormat.format(date);
                            Context context = getApplicationContext();

                            Intent notificationIntent = new Intent(context, AlternativeProgress.class);
                            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            PendingIntent contentIntent = PendingIntent.getActivity(context,
                                    0, notificationIntent,
                                    PendingIntent.FLAG_CANCEL_CURRENT);

                            Resources res = context.getResources();
                            Notification.Builder builder = new Notification.Builder(context);

                            builder.setContentIntent(contentIntent)
                                    .setSmallIcon(R.mipmap.potlast)
                                    //.setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.hungrycat))
                                    //.setTicker("Последнее предупреждение!")
                                    .setWhen(System.currentTimeMillis())
                                    .setAutoCancel(true)
                                    .setContentTitle("Таймер")
                                    .setContentText(dateText);

                            Notification notification = builder.getNotification(); // до API 16
                            //Notification notification = builder.build();

                            NotificationManager notificationManager = (NotificationManager) context
                                    .getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(11, notification);

                        } else {
                            Context context = getApplicationContext();
                            NotificationManager notificationManager = (NotificationManager) context
                                    .getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.cancel(11);
                            timerIsWorking = false;


                            setVisibility(false);
                            setClickable(true);
                            mExerciseTitle.setText(getString(titles[positionInArray]));
                            timer.cancel();
                            mCounter.setText(String.valueOf(0));
                            timeSec = null;
                            ObjectAnimator fadeOutAnimationAlphaWeight = ObjectAnimator.ofFloat(mWeightEdit, View.ALPHA, 1).setDuration(800);
                            ObjectAnimator fadeOutAnimationTranslateWeight = ObjectAnimator.ofFloat(mWeightEdit, View.TRANSLATION_X, 0).setDuration(800);
                            ObjectAnimator fadeOutAnimationAlphaRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.ALPHA, 1).setDuration(800);
                            ObjectAnimator fadeOutAnimationTranslateRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.TRANSLATION_X, 0).setDuration(800);
                            ObjectAnimator fadeOutAnimationAlphaTime = ObjectAnimator.ofFloat(mTimeEdit, View.ALPHA, 1).setDuration(800);
                            ObjectAnimator fadeOutAnimationTranslateTime = ObjectAnimator.ofFloat(mTimeEdit, View.TRANSLATION_X, 0).setDuration(800);
                            AnimatorSet animatorSet = new AnimatorSet();
                            animatorSet.playTogether(fadeOutAnimationAlphaWeight, fadeOutAnimationTranslateWeight, fadeOutAnimationAlphaRepeat, fadeOutAnimationTranslateRepeat, fadeOutAnimationAlphaTime, fadeOutAnimationTranslateTime);
                            animatorSet.start();
//                            mSlider.setVisibility(View.INVISIBLE);
                            mCounter.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        },0, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (timer != null)
//            timer.cancel();
    }

    @Override
    public void onBackPressed() {
        if (!timerIsWorking){
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = this.getTheme();
            theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
            final @ColorInt int color = typedValue.data;
            MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                    .titleColor(color)
                    .positiveColor(color)
                    .neutralColor(color)
                    .negativeColor(color)
                    .title(R.string.leave_process_warning)
                    .positiveText(R.string.dialog_leave_positive)
                    .negativeText(R.string.dialog_leave_neagative)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent intent = new Intent(AlternativeProgress.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.cancel();
                        }
                    })
                    .build();
            dialog.show();
        } else {
            Toast.makeText(this, R.string.timer_working_warning, Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rlButtonApproach:
                getPositionInArray();
                if (mWeightEdit.getText().toString().isEmpty() || mRepeatEdit.getText().toString().isEmpty() || mTimeEdit.getText().toString().isEmpty()){
                    Toast.makeText(this, R.string.editText_warning, Toast.LENGTH_SHORT).show();
                } else {
                    setAnimation();
                    timeSec = System.currentTimeMillis();
                    counter = Integer.parseInt(mTimeEdit.getText().toString());
                    startTimer();
                    weight = Integer.parseInt(mWeightEdit.getText().toString()); repeats = Integer.parseInt(mRepeatEdit.getText().toString());
                    GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
                    SQLiteDatabase db = gymTrackDatabaseHelper.getWritableDatabase();
                    gymTrackDatabaseHelper.insertResult(db, searchInArray, approach, repeats, weight, id_progress);
                    Cursor cursor = db.query("result", new String[]{"id_result"}, null, null, null, null, null);
                    if (cursor.moveToLast()) id_result = cursor.getInt(0);
                    cursor.close();
                    gymTrackDatabaseHelper.insertProgressResult(db, id_progress, id_result);
                    db.close();
                    approach = approach + 1;
                    mApproach.setText(approach + " " + getResources().getString(R.string.approach_text));
                }
                break;
            case R.id.rlButtonExecise:
                mWeightEdit.setText(""); mRepeatEdit.setText("");
                biasInArray();
                if (titles.length == 0){
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("theme", theme);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    this.finish();
                } else {
                    approach = 1;
                    searchInArray = id[0];
                    setRecyclerView(true);
                    mApproach.setText(approach + " " + getResources().getString(R.string.approach_text));
                    mExerciseTitle.setText(getString(titles[0]));
                    positionInArray = 0;
                }
                break;
            case R.id.circletime_view:
                setVisibility(false);
                setClickable(true);
                mExerciseTitle.setText(getString(titles[positionInArray]));
                timer.cancel();
                mCounter.setText(String.valueOf(0));
                timeSec = null;
                Context context = getApplicationContext();
                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(11);
                timerIsWorking = false;
                ObjectAnimator fadeOutAnimationAlphaWeight = ObjectAnimator.ofFloat(mWeightEdit, View.ALPHA, 1).setDuration(1000);
                ObjectAnimator fadeOutAnimationTranslateWeight = ObjectAnimator.ofFloat(mWeightEdit, View.TRANSLATION_X, 0).setDuration(1000);
                ObjectAnimator fadeOutAnimationAlphaRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.ALPHA, 1).setDuration(1000);
                ObjectAnimator fadeOutAnimationTranslateRepeat = ObjectAnimator.ofFloat(mRepeatEdit, View.TRANSLATION_X, 0).setDuration(1000);
                ObjectAnimator fadeOutAnimationAlphaTime = ObjectAnimator.ofFloat(mTimeEdit, View.ALPHA, 1).setDuration(1000);
                ObjectAnimator fadeOutAnimationTranslateTime = ObjectAnimator.ofFloat(mTimeEdit, View.TRANSLATION_X, 0).setDuration(1000);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(fadeOutAnimationAlphaWeight, fadeOutAnimationTranslateWeight, fadeOutAnimationAlphaRepeat, fadeOutAnimationTranslateRepeat, fadeOutAnimationAlphaTime, fadeOutAnimationTranslateTime);
                animatorSet.start();
                mCounter.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void onClickTitle(View view){
        ArrayList<String> repeats = new ArrayList<>(), weight=new ArrayList<>();
        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query("result",
                new String[]{"repeats", "weight"},
                "ID_EXERCISE = ?",
                new String[]{Integer.toString(id[bigPosition])},
                null,null,null);
        if(cursor.moveToFirst()){
            repeats.add(Integer.toString(cursor.getInt(0)));
            weight.add(Integer.toString(cursor.getInt(1)));
            while (cursor.moveToNext()){
                repeats.add(Integer.toString(cursor.getInt(0)));
                weight.add(Integer.toString(cursor.getInt(1)));
            }
        }
        String buf="";
        for (int i = 0; i<weight.size();i++){
            buf+=weight.get(i)+"x"+repeats.get(i)+"\n";
        }
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = this.getTheme();
        theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
        final @ColorInt int color = typedValue.data;
        new MaterialDialog.Builder(this)
                .titleColor(color)
                .positiveColor(color)
                .neutralColor(color)
                .negativeColor(color)
                .title("Предыдущие упражнения")
                .content(buf)
                .positiveText("OK")
                .show();
    }

    public int getTableLength(String tableName, String whereName, int searchName){
        SQLiteOpenHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(this);
        SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
        Cursor cursorLength= db.query(tableName,
                new String[]{ "COUNT(*) AS numb"},
                whereName +" = ?",
                new String[]{Integer.toString(searchName)},
                null, null, null);
        int tableLeng = 0;
        if (cursorLength.moveToFirst()) {
            tableLeng = cursorLength.getInt(0);
        }
        cursorLength.close();
        db.close();
        return tableLeng;
    }
}
