package com.example.kbeatis.gymtrack;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by kbeatis on 19.03.17.
 */

public class TrainingAdapter extends  RecyclerView.Adapter<TrainingAdapter.ViewHolder>{
    private String[] program_title, day_title, date;
    private Listener listener;
    public static  interface Listener{
        public void onClick(int position);
    }

    public void setListener(Listener listener){
        this.listener=listener;
    }

    public TrainingAdapter(String[] program_title, String[] day_title, String[] date){
        this.program_title = program_title;
        this.day_title = day_title;
        this.date = date;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout v){
            super(v);
            cardView = v;
        }
    }

    @Override
    public TrainingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_training, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(TrainingAdapter.ViewHolder holder,final int position) {
        RelativeLayout cardView = holder.cardView;
        TextView mProgramTitle = (TextView)cardView.findViewById(R.id.programTitle);
        TextView mDayTitle = (TextView)cardView.findViewById(R.id.dayTitle);
        TextView mDate = (TextView)cardView.findViewById(R.id.dateTitle);
        mProgramTitle.setText(program_title[position]);
        mDayTitle.setText(day_title[position]);
        mDate.setText(date[position]);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener!=null) listener.onClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return date.length;
    }
}
