package com.example.kbeatis.gymtrack;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class GraphFragment extends Fragment {

    private int tableLength;
    private int[] id;
    private int[] titles;
    private int[] idImageResource;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(R.string.nav_graph);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView graphRecycler = (RecyclerView) inflater
                .inflate(R.layout.fragment_graph, container, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        try {
            GymTrackDatabaseHelper gymTrackDatabaseHelper = new GymTrackDatabaseHelper(getActivity());
            tableLength = gymTrackDatabaseHelper.getTableLength("MUSCLE", null, null);
            id = new int[tableLength];
            titles = new int[tableLength];
            idImageResource = new int[tableLength];
            SQLiteDatabase db = gymTrackDatabaseHelper.getReadableDatabase();
            Cursor cursor;
            for (int i = 1;i < id.length+1; i++) {
                cursor = db.query("MUSCLE",
                        new String[]{ "TITLE", "ID_IMAGE_RESOURCE"},
                        "ID_MUSCLE = ?",
                        new String[] {Integer.toString(i)},
                        null, null, null);
                if (cursor.moveToFirst()) {
                    id[i-1] = i;
                    titles[i-1] = cursor.getInt(0);
                    idImageResource[i-1] = cursor.getInt(1);
                }
                cursor.close();
            }
            db.close();
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(getActivity(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        GraphAdapter adapter = new GraphAdapter(titles, idImageResource);
        graphRecycler.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        graphRecycler.setLayoutManager(layoutManager);

        adapter.setListener(new GraphAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Fragment fragment = new GraphExerciseFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment, "visible_fragment");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();
            }
        });
        return graphRecycler;
    }
}
