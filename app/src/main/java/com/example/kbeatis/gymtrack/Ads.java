package com.example.kbeatis.gymtrack;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Konstantin on 05.04.2017.
 */

public class Ads {
    // замените на реальное значение высоты баннера
    final private static int BANNER_HEIGHT = 75;

    public static void showBottomBanner(final Activity activity)  {
        // замените на реальный код для работы с вашим рекламным сервисом
/*
        final Banner banner = new Banner(activity);

        banner.setBannerListener(new BannerListener() {
            @Override
            public void onReceiveAd(View view) {
                // добавить отступ снизу
                setupContentViewPadding(activity, true, BANNER_HEIGHT);
            }
        });

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                height);//  Utils.toDIP(activity, BANNER_HEIGHT));

        layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        activity.getWindow().addContentView(banner, layoutParams);*/
    }

    public static void setupContentViewPadding(Activity activity, boolean top, int margin) {
        View view = ((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content)).getChildAt(0);
        if (top)
            view.setPadding(view.getPaddingLeft(), margin, view.getPaddingRight(), view.getPaddingBottom());
        else
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), margin);
    }
}