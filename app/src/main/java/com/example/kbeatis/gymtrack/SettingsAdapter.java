package com.example.kbeatis.gymtrack;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Konstantin on 31.03.2017.
 */

public class SettingsAdapter extends RecyclerView.Adapter <SettingsAdapter.ViewHolder> {

    private String[] titles;
    private SettingsAdapter.Listener listener;

    public static interface Listener{
        public void onClick(int position);
    }

    public void setListener (SettingsAdapter.Listener listener){
        this.listener = listener;
    }

    public SettingsAdapter(String[] titles){
        this.titles = titles;
    }
    public static class  ViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout cardView;
        public ViewHolder(RelativeLayout view) {
            super(view);
            cardView = view;
        }
    }

    @Override
    public SettingsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout cardView = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_settings, parent, false);
        return new SettingsAdapter.ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(SettingsAdapter.ViewHolder holder, final int position) {
        RelativeLayout cardView = holder.cardView;

        TextView textView = (TextView)cardView.findViewById(R.id.settings_text);
        textView.setText(titles[position]);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }
}
