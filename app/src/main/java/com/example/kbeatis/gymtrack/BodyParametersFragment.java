package com.example.kbeatis.gymtrack;


import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class BodyParametersFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    Toolbar toolbar;

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setTitle(getString(R.string.nav_settings));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_body_parameters, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.body_parameters_recycler);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        String[] titles = {"Вес", "Рост"};
        BodyParametersAdapter adapter = new BodyParametersAdapter(titles);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getContext().getTheme();
        theme.resolveAttribute(R.attr.text_color_black, typedValue, true);
        final @ColorInt int color = typedValue.data;

        adapter.setListener(new BodyParametersAdapter.Listener(){
            @Override
            public void onClick(int position) {
                switch (position){
                    case 0:
                        new MaterialDialog.Builder(getContext())
                                .titleColor(color)
                                .widgetColor(color)
                                .positiveColor(color)
                                .neutralColor(color)
                                .negativeColor(color)
                                .title("Вес")
                                //.content("Текст")
                                .inputType(InputType.TYPE_CLASS_NUMBER)
                                .input("Введите вес", null, new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                        Calendar calendar = Calendar.getInstance();
                                        String date = String.valueOf(calendar.get(Calendar.YEAR)+ "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.DATE));
                                        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(getContext());
                                        SQLiteDatabase db = helper.getWritableDatabase();
                                        helper.insertHeight(db, Float.parseFloat(input.toString()), date);
                                    }
                                }).show();
                        break;
                    case 1:
                        new MaterialDialog.Builder(getContext())
                                .titleColor(color)
                                .widgetColor(color)
                                .positiveColor(color)
                                .neutralColor(color)
                                .negativeColor(color)
                                .title("Рост")
                                //.content("Текст")
                                .inputType(InputType.TYPE_CLASS_NUMBER)
                                .input("Введите рост", null, new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                        Calendar calendar = Calendar.getInstance();
                                        String date = String.valueOf(calendar.get(Calendar.YEAR)+ "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.DATE));
                                        GymTrackDatabaseHelper helper = new GymTrackDatabaseHelper(getContext());
                                        SQLiteDatabase db = helper.getWritableDatabase();
                                        helper.insertWeight(db, Float.parseFloat(input.toString()), date);
                                    }
                                }).show();
                        break;
                }
            }
        });
        return view;
    }
private void inputBD(){
    Calendar calendar = Calendar.getInstance();
    String date = String.valueOf(calendar.get(Calendar.YEAR)+ "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.DATE));
}
}
