package com.example.kbeatis.gymtrack;

/**
 * Created by kbeatis on 25.03.17.
 */
import com.squareup.otto.Bus;

public class BusProvider {

    private static final Bus BUS = new Bus();

    public static Bus getInstance(){return BUS;}

    private BusProvider(){}
}
